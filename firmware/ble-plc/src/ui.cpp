#include <new>
#include <cstring>

#include "ecpp/String.hpp"
#include "bluetooth.hpp"
#include "bsp.hpp"
#include "plc.hpp"
#include "ui.hpp"

using namespace std;
using namespace ecpp;


class SetupMenu;


Ui Ui::s_instance;

void
Ui::init()
{
  stack_depth = 0;
  new ( get_widget_ptr(0)) IdleWidget();

  invalidated = true;
}


void
Ui::poll()
{
  auto & bsp = Bsp::get_instance();
  Bsp::KeyState key;

  /* process keyboard */
  key = bsp.get_key_state();

  if(last_key_state != key)
  { /* key changes */
    widget_idle_timer.startMilliseconds(widget_idle_timeout);

    if (key != Bsp::KeyState::ButtonNone)
    {
      last_key_state = key;
    }
    else
    {
      key = last_key_state;
      last_key_state = Bsp::KeyState::ButtonNone;
    }

    get_current_widget().key_event(key, (last_key_state == key) ? 1 : 0);
  }
  else if (key != Bsp::KeyState::ButtonNone)
  { /* long key presses */
    auto ticks = bsp.getTicksSinceKeyChange();

    if(ticks > 1)
      get_current_widget().key_event(key, 1 + ticks);
  }

  /* handle pop-ups if required */
  if (stack_depth == 0)
  {
    if (bluetooth_state_changed)
    {
      if(nullptr != push_widget<BluetoothInfoWidget>())
        bluetooth_state_changed = false;
    }
    else
    {
      bsp.set_led_off();
    }
  }
  else if (widget_idle_timer.hasTimedOut())
  {
    while(stack_depth > 0)
      pop_widget(get_widget_ptr(stack_depth));
  }
  else
  {
    bsp.set_led_green();
  }
}

void
Ui::draw(Lcd &lcd)
{
  invalidated = false;
  get_current_widget().draw();
}

void LcdWidget::key_event(Bsp::KeyState key, int_fast16_t pressed)
{ /* dummy implementation, do nothing */

}

void
LcdWidget::state_changed(void *p)
{
  if( &(Bsp::get_instance().clock) == p)
    Ui::get_instance().invalidate();
}

void BluetoothInfoWidget::draw()
{
  auto & lcd  = Bsp::get_instance().lcd;
  auto & blue = Bluetooth::get_instance();
  char *row;

  row = reinterpret_cast<char*>(lcd.getRow(0));

  strncpy(row, "Bluetooth ", 10);

  if (NRF_SUCCESS != blue.get_status().last_error_rc)
  {
    row = reinterpret_cast<char*>(lcd.getRow(1));
    strncpy(row, "  Error   ", 10);

    row = reinterpret_cast<char*>(lcd.getRow(2));
    memset(row,' ', 10);
    String::convertToHex(row + 3, 4, blue.get_status().last_error_rc);
  }
  else
  {
    row = reinterpret_cast<char*>(lcd.getRow(1));
    strncpy(row, "    OK    ", 10);

    row = reinterpret_cast<char*>(lcd.getRow(2));
    memset(row,' ', 10);
  }

  lcd.getRowCommand(3).setText(blue.getRxChars(), 10);
}

void
BluetoothInfoWidget::key_event(Bsp::KeyState key, int_fast16_t pressed)
{ /* dummy implementation, do nothing */
  if(pressed == 1)
    Ui::get_instance().pop_widget(this);
}

void Ui::state_changed(void *p)
{
  if( &(Bluetooth::get_instance()) == p)
    bluetooth_state_changed = true;

  get_current_widget_ptr()->state_changed(p);
}

void
Ui::pop_widget(LcdWidget * widget)
{
  if (stack_depth < 1)
    return;

  if (widget == get_widget_ptr(stack_depth))
  {
    stack_depth--;

    /* Normally now call destructor, but since we dont need destruction
     * in almost any case here, lets skip that step
     *
     * widget->~LcdWidget();
     */

    invalidate();
  }
}

static const char* s_mainmenu_items[] =
{
  "Status",
  "Einstell.",
  "Program",
  "Setup",
  "Bluetooth",
};

class MainMenu : public MenuScrollWidget
{
public:
  MainMenu() : MenuScrollWidget("Hauptmen�", s_mainmenu_items, ElementCount(s_mainmenu_items)) {};
  virtual void press_item(uint_least8_t idx);
};

void
MainMenu::press_item(uint_least8_t idx)
{
  switch(idx)
  {
    case 0:
      Ui::get_instance().push_widget<LocalStatusScrollWidget>();
      break;
    case 1:
      Ui::get_instance().push_widget<ManagePlcProgramParametersWidget>();
      break;
    case 2:
      Ui::get_instance().push_widget<ManagePlcWidget>();
      break;
    case 3:
      Ui::get_instance().push_widget<SetupMenu>();
      break;
    case 4:
      Ui::get_instance().push_widget<BluetoothInfoWidget>();
      break;
  }
}

static const char* s_setupmenu_items[] =
{
  "Zeit",
};

class SetupMenu : public MenuScrollWidget
{
public:
  SetupMenu() : MenuScrollWidget("Einstellungen", s_setupmenu_items, ElementCount(s_setupmenu_items)) {};
  virtual void press_item(uint_least8_t idx);
};

void
SetupMenu::press_item(uint_least8_t idx)
{
  switch(idx)
  {
    case 0:
      Ui::get_instance().push_widget<SetDateTimeWidget>();
      break;
  }
}

static const char* s_PlcStateNames[] =
{
  [Plc::PlcState::Poweron]       = "Start",
  [Plc::PlcState::LoadingEEProm] = "Laden",
  [Plc::PlcState::Loaded]        = "Pr�fen",
  [Plc::PlcState::Stopped]       = "Stop",
  [Plc::PlcState::Running]       = "Betrieb",
  [Plc::PlcState::LoadError]     = "Fehler",
};

void IdleWidget::draw()
{
  auto & bsp   = Bsp::get_instance();
  auto & lcd   = bsp.lcd;
  auto & plc   = Plc::get_instance();
  auto & clock = Bsp::get_instance().clock;


  { /* 1. row */
    auto & row = lcd.getRowCommand(0).text;

    formatTime( clock.getTime(), *reinterpret_cast<char (*) [8]>(row + 1));
  }

  { /* 2. row */
    auto & row = lcd.getRowCommand(1).text;
    formatDate( clock.getDate(), *reinterpret_cast<char (*) [10]>(row));
  }

  /* 3. row */
  lcd.getRowCommand(2).setTextCentered(s_PlcStateNames[plc.getState()], 10);

  { /* 4. row */
    auto & row = lcd.getRowCommand(3).text;
    int i;

    memcpy(row, " ________ ", 10);
    for(i = 0; i < 8; ++i)
    {
      if (bsp.get_dq(i))
        row[i+1] = '0' + i;
    }
  }
}

void
IdleWidget::key_event(Bsp::KeyState key, int_fast16_t pressed)
{
  if(1 == pressed)
    Ui::get_instance().push_widget<MainMenu>();
}



void
LocalStatusScrollWidget::draw_item(uint_least8_t idx)
{
  auto & bsp = Bsp::get_instance();
  auto & lcd = bsp.lcd;
  auto & plc = Plc::get_instance();
  char *row;
  uint_fast8_t i;

  switch(idx)
  {
  case 0:
    row = reinterpret_cast<char*>(lcd.getRow(1));
    strncpy(row, " Ausg�nge ", 10);

    row = reinterpret_cast<char*>(lcd.getRow(2));
    strncpy(row, " ________ ", 10);

    for(i = 0; i < 8; ++i)
    {
      if (bsp.get_dq(i))
        row[i+1] = '0' + i;
    }
    break;
  case 1:
  case 2:
  case 3:
  case 4:
    row = reinterpret_cast<char*>(lcd.getRow(1));
    strncpy(row, "Eingang X ", 10);
    row[8] = '0' + idx - 1;

    row = reinterpret_cast<char*>(lcd.getRow(2));
    strncpy(row, "        V ", 10);
    String::formatSigned(row + 1, 5, plc.get_ai(idx - 1).value.get_raw());
    String::formatFrac  (row + 1, 6, 3);
    break;
  case 5:
    row = reinterpret_cast<char*>(lcd.getRow(1));
    strncpy(row, "Versorgung", 10);

    row = reinterpret_cast<char*>(lcd.getRow(2));
    strncpy(row, "        V ", 10);
    String::formatSigned(row + 1, 5, plc.get_asupply().value.get_raw());
    String::formatFrac(row + 1, 6, 3);
    break;
  }
}

void
LocalStatusScrollWidget::state_changed(void *p)
{
  if( &(Plc::get_instance()) == p)
    Ui::get_instance().invalidate();

  ScrollWidget::state_changed(p);
}

void
SetDateTimeWidget::draw_item(uint_least8_t idx)
{
  auto & bsp = Bsp::get_instance();
  auto & lcd = bsp.lcd;
  char *row;

  row = reinterpret_cast<char*>(lcd.getRow(1));
  memset(row,' ', 10);
  formatDate( bsp.clock.getDate(), *reinterpret_cast<char (*) [10]>(row));

  row = reinterpret_cast<char*>(lcd.getRow(2));
  memset(row,' ', 10);
  formatTime( bsp.clock.getTime(), *reinterpret_cast<char (*) [8]>(row + 1));

  switch(idx)
  {
  case 0: lcd.setCursor(3,1); break;
  case 1: lcd.setCursor(6,1); break;
  case 2: lcd.setCursor(9,1); break;
  case 3: lcd.setCursor(2,2); break;
  case 4: lcd.setCursor(5,2); break;
  }
}

void
SetDateTimeWidget::key_event(Bsp::KeyState key, int_fast16_t pressed)
{
  if (key == Bsp::KeyState::Button4)
  {
    if(pressed == 1)
    {
      auto & clock = Bsp::get_instance().clock;

      switch(position)
      {
      case 0:
        if (clock.getYear() < (clock.getDate().YearStart + 99))
          clock.set(clock.makeDate(clock.getYear() + 1, clock.getMonth(), clock.getDay()));
        else
          clock.set(clock.makeDate(clock.getDate().YearStart, clock.getMonth(), clock.getDay()));
        break;
      case 1:
        if (clock.getMonth() < 11 )
          clock.set(clock.makeDate(clock.getYear(), clock.getMonth() + 1, clock.getDay()));
        else
          clock.set(clock.makeDate(clock.getYear(), 0, clock.getDay()));
        break;
      case 2:
        if ((clock.getDay() + 1) < clock.getDate().getDaysInMonth() )
          clock.set(clock.makeDate(clock.getYear(), clock.getMonth(), clock.getDay() + 1));
        else
          clock.set(clock.makeDate(clock.getYear(), clock.getMonth(), 0));
        break;
      case 3:
        Plc::get_instance().handleTimeChanged();

        if (clock.getHour() < 23)
          clock.set(clock.makeTime(clock.getHour() + 1, clock.getMinute(), 0 ));
        else
          clock.set(clock.makeTime(0, clock.getMinute(), 0 ));
        break;
      case 4:
        Plc::get_instance().handleTimeChanged();

        if (clock.getMinute() < 59)
          clock.set(clock.makeTime(clock.getHour(), clock.getMinute() + 1, 0 ));
        else
          clock.set(clock.makeTime(clock.getHour(), 0, 0 ));
        break;
      }

      Ui::get_instance().invalidate();
    }
  }
  else
  {
    ScrollWidget::key_event(key,pressed);
  }
}

static const char* s_PlcStateChanges[] =
{
  [Plc::PlcState::Poweron]       = "Starte",
  [Plc::PlcState::LoadingEEProm] = "Lade",
  [Plc::PlcState::Loaded]        = "Starte",
  [Plc::PlcState::Stopped]       = "Starte",
  [Plc::PlcState::Running]       = "Stoppe",
  [Plc::PlcState::LoadError]     = "Fehler",
};

void
ManagePlcWidget::draw_item(uint_least8_t idx)
{
  auto & plc = Plc::get_instance();
  auto & eep = Bsp::get_instance().getEepromController();
  auto & lcd = Bsp::get_instance().lcd;

  switch(idx)
  {
  case 0:
    lcd.getRowCommand(1).setTextCentered(s_PlcStateNames[plc.getState()], 10);

    if(plc.getState() != plc.getRequestedState())
    {
      lcd.getRowCommand(2).setTextCentered(s_PlcStateChanges[plc.getState()], 10);
    }
    else
    {
      if(Plc::PlcState::Running == plc.getRequestedState())
        lcd.getRowCommand(2).setTextCentered("<Stop>", 10);
      else
        lcd.getRowCommand(2).setTextCentered("<Start>", 10);
    }
    break;
  case 1:
    lcd.getRowCommand(1).setTextCentered("Speichern", 10);

    if(Plc::PlcState::Stopped != plc.getState())
    {
      lcd.getRowCommand(2).setTextCentered("Blockiert", 10);
    }
    else if(eep.state == TwiEepromController::State::Idle)
    {
      if (hint == Hint::EEPFailure)
        lcd.getRowCommand(2).setTextCentered("Fehler", 10);
      else if (hint == Hint::EEPSuccess)
        lcd.getRowCommand(2).setTextCentered("Fertig", 10);
      else
        lcd.getRowCommand(2).setTextCentered("<Sichern>", 10);
    }
    else
    {
      last_progress = lcd.getRowCommand(2).makeProgress(eep.offset, eep.length);
    }
    break;

  case 2:
    lcd.getRowCommand(1).setTextCentered("Laden", 10);

    if(Plc::PlcState::Running == plc.getState())
    {
      lcd.getRowCommand(2).setTextCentered("Blockiert", 10);
    }
    else if(eep.state == TwiEepromController::State::Idle)
    {
      if (hint == Hint::EEPFailure)
        lcd.getRowCommand(2).setTextCentered("Fehler", 10);
      else if (hint == Hint::EEPSuccess)
        lcd.getRowCommand(2).setTextCentered("Fertig", 10);
      else
        lcd.getRowCommand(2).setTextCentered("<Laden>", 10);
    }
    else
    {
      last_progress = lcd.getRowCommand(2).makeProgress(eep.offset, eep.length);
    }
    break;
  }
}

void
ManagePlcWidget::key_event(Bsp::KeyState key, int_fast16_t pressed)
{
  if (key != Bsp::KeyState::Button4)
  {
    hint = Hint::None;
    ScrollWidget::key_event(key, pressed);
    return;
  }

  if(pressed != 1)
    return;

  auto & plc = Plc::get_instance();
  auto & eep = Bsp::get_instance().getEepromController();

  switch(position)
  {
  case 0:
    if(Plc::PlcState::Running == plc.getRequestedState())
      plc.setRequestState(Plc::PlcState::Stopped);
    else
      plc.setRequestState(Plc::PlcState::Running);
    break;
  case 1:
    if(eep.state == TwiEepromController::State::Idle)
      plc.storeEEPromProgram();
    break;
  case 2:
    if(eep.state == TwiEepromController::State::Idle)
      plc.loadEEPromProgram();
    break;
  };
}

void
ManagePlcWidget::state_changed(void *p)
{
  if( &(Plc::get_instance()) == p)
    Ui::get_instance().invalidate();

  if( &(Bsp::get_instance().getEepromController()) == p)
  {
    auto & eep = Bsp::get_instance().getEepromController();

    if(TwiEepromController::State::Idle == eep.state)
    {
      if(eep.length == eep.offset)
        hint = Hint::EEPSuccess;
      else
        hint = Hint::EEPFailure;

      Ui::get_instance().invalidate(this);
    }
    else
    {
      if (last_progress != Lcd::RowCommand::calcProgress(eep.offset, eep.length))
        Ui::get_instance().invalidate(this);
    }
  }

  ScrollWidget::state_changed(p);
}

::PlcApplication::VariableManager*
UiParameterInterface::createVariableManager(const ::PlcApplication::PlcVariableMetaBase &meta, const char* name, void *var_ptr)
{
  switch (meta.type_id)
  {
  case ::PlcApplication::PlcTypeId<uint8_t>::type_id:
    return newVariableManager<::PlcApplication::IntegerVariableManager<uint8_t> >(meta, name, var_ptr);

  case ::PlcApplication::PlcTypeId<::PlcApplication::AnalogInput::AnalogValue>::type_id:
    return newVariableManager<::PlcApplication::AnalogVariableManager >(meta, name, var_ptr);

  case ::PlcApplication::PlcTypeId<::PlcApplication::PlcDateType>::type_id:
    return newVariableManager<::PlcApplication::DateVariableManager>(meta, name, var_ptr);

  case ::PlcApplication::PlcEnumerationTypeId<uint8_t>::type_id:
    return newVariableManager<::PlcApplication::EnumerationVariableManager<uint8_t> >(meta, name, var_ptr);

  default:
    return nullptr;
  }
}

ManagePlcProgramParametersWidget::ManagePlcProgramParametersWidget() : ScrollWidget("Parameter", 0), var_mgr_ptr(nullptr), param_intf()
{
  auto & plc = Plc::get_instance();

  entries  = plc.getParameterCount();
  position = 0;

  if(entries > 0)
    var_mgr_ptr = plc.createParameterManager(param_intf, position);
}

void
ManagePlcProgramParametersWidget::draw_item(uint_least8_t idx)
{
  auto & lcd = Bsp::get_instance().lcd;

  if(nullptr != var_mgr_ptr)
  {
    lcd.getRowCommand(1).setTextCentered(var_mgr_ptr->getName(),10);
    var_mgr_ptr->formatValue(var_mgr_ptr->getVarPtr(),
                             reinterpret_cast<char*>(lcd.getRowCommand(2).text),10);
  }
}

void
ManagePlcProgramParametersWidget::press_item(uint_least8_t idx)
{
  if(nullptr == var_mgr_ptr)
    return;

  if(sizeof(ConfigurePlcProgramParameterWidget::var_mem) < var_mgr_ptr->getVarSize())
    return;

  Ui::get_instance().push_widget<ConfigurePlcProgramParameterWidget>(*var_mgr_ptr);
}

void
ManagePlcProgramParametersWidget::key_event(Bsp::KeyState key, int_fast16_t pressed)
{
  ScrollWidget::key_event(key, pressed);

  if(entries > position)
  {
    auto & plc = Plc::get_instance();
    var_mgr_ptr = plc.createParameterManager(param_intf, position);
  }
}

void
ManagePlcProgramParametersWidget::state_changed(void *p)
{
  if(p == &(Plc::get_instance()))
    Ui::get_instance().invalidate(this);
}

ConfigurePlcProgramParameterWidget::ConfigurePlcProgramParameterWidget(::PlcApplication::VariableManager & var_mgr) :
  var_mgr(var_mgr), var_mem()
{
  var_mgr.copyValue(var_mem.buf, var_mgr.getVarPtr());
}


void
ConfigurePlcProgramParameterWidget::draw()
{
  auto & lcd = Bsp::get_instance().lcd;

  lcd.getRowCommand(0).setTextCentered(var_mgr.getName(), 10);

  var_mgr.formatValue(var_mgr.getVarPtr(), reinterpret_cast<char*>(lcd.getRowCommand(1).text) + 1, 9);
  var_mgr.formatValue(var_mem.buf,         reinterpret_cast<char*>(lcd.getRowCommand(2).text) + 1, 9);

  lcd.getRowCommand(3).setText("X |-  +|OK", 10);
}

void
ConfigurePlcProgramParameterWidget::key_event(Bsp::KeyState key, int_fast16_t pressed)
{
  if (pressed != 1)
    return;

  switch(key)
  {
  case Bsp::KeyState::Button1:
    Ui::get_instance().pop_widget(this);
    break;
  case Bsp::KeyState::Button2:
    var_mgr.decreaseValue(var_mem.buf);
    break;
  case Bsp::KeyState::Button3:
    var_mgr.increaseValue(var_mem.buf);
    break;
  case Bsp::KeyState::Button4:
    var_mgr.setVar(var_mem.buf);
    break;
  case Bsp::KeyState::ButtonNone:
    break;
  }

  Ui::get_instance().invalidate(this);
}

void
ConfigurePlcProgramParameterWidget::state_changed(void *p)
{

}
