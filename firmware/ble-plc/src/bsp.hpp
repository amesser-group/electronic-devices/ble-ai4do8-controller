#ifndef BSP_HPP_
#define BSP_HPP_

#include <cstdint>
#include "ecpp/Time.hpp"
#include "ecpp/Peripherals/Keyboard.hpp"

#include "hal/nrf_gpio.h"
#include "nrfx_saadc.h"
#include "nrfx_pwm.h"
#include "nrfx_systick.h"
#include "nrfx_twim.h"
#include "nrfx_power.h"
#include "nrfx_rtc.h"
#include "nrfx_timer.h"
#include "nrf_nvic.h"

class Lcd
{
public:
    static constexpr uint_fast8_t columns = 10;
    static constexpr uint_fast8_t rows    =  4;

    typedef uint8_t RowText[columns];

    struct RowCommand
    {
      static constexpr uint_least8_t getLength() {return 3+columns; }

      constexpr const uint8_t* getData() const {return control;}

      uint8_t control[3];

      uint8_t text[columns];
      /* one extra byte to consume zero termination if any */
      uint8_t terminator;

      void setText(const char *text, uint_fast8_t textlen);
      void setTextCentered(const char *text, uint_fast8_t textlen);

      static uint_fast8_t calcProgress(uint_fast32_t done, uint_fast32_t amount);
      uint_fast8_t makeProgress(uint_fast32_t done, uint_fast32_t amount);
    };

    struct ExtraCommand
    {
      static constexpr uint_least8_t getLength() {return 2 ;}

      constexpr const uint8_t* getData() const {return data;}

      uint8_t  data[2];
    };

  static constexpr uint_least8_t cmd_control_continue = 0x80;
  static constexpr uint_least8_t cmd_control_command  = 0x00;
  static constexpr uint_least8_t cmd_control_data     = 0x40;
  static constexpr uint_least8_t cmd_command_locate   = 0x80;

private:
  RowCommand     display_data[rows];

  /** Extra data to send to display after transferringdisplay data */
  ExtraCommand   extra_data;

  uint_least8_t  repaint;

  constexpr uint8_t makeLocateCommand(uint_fast8_t col, uint_fast8_t row) const {return cmd_command_locate | (row * 0x20 + 10 + col); }

  void remapCharacters();

public:
  void init();

  RowText & getRow(uint_fast8_t row) {return display_data[row].text;}

  RowCommand   & getRowCommand(uint_fast8_t row) {return display_data[row];}
  ExtraCommand & getExtraCommand() {return extra_data;}

  void clear();
  void setCursor(uint_fast8_t col, uint_fast8_t row) {extra_data.data[1] = makeLocateCommand(col,row); }

  void invalidate() {repaint = true;}

  friend class TwiLcdController;
};

class TwiTransferController
{
public:
  enum TransferStatus : uint_least8_t
  {
    Sucessfull = 0,
    DataNak    = 1,
    AddressNak = 2,
  };

  constexpr TwiTransferController() : state() {};

  /** called by bsp when transfer started */
  virtual void start();
  /** called by bsp when twi request is finished */
  virtual void done(TransferStatus status);
protected:
  /** invoke to request a twi write */
  void startWrite(uint_least8_t addr, const void* data, size_t datalen, bool send_stop = true);
  /** invoke to request a twi read */
  void startRead(uint_least8_t addr, void* data, size_t datalen);
  /** invoke to request a write/Read */
  void startWriteRead(uint_least8_t addr, void* write_data, size_t write_len, void* read_data, size_t read_len);

  volatile uint_least8_t state;
};

class TwiLcdController : public TwiTransferController
{
private:
  volatile uint_least8_t state;
public:
  constexpr TwiLcdController() : TwiTransferController(), state(0) {};

  bool pending() const;

  void start() override ;
  void done(TransferStatus status) override;

  void init();

  friend class Bsp;
};

class TwiEepromController : public TwiTransferController
{
private:
  enum State : uint_least8_t
  {
    Idle,
    ReadReq,
    ReadPend,
    WriteReq,
    WritePend,
  };

  uint8_t                *buffer;
  uint_least16_t          length;
  uint_least16_t          offset;
  volatile State          state;

  uint8_t                 databuf[2 + 32];
public:
  constexpr TwiEepromController() : TwiTransferController(), buffer(nullptr), length(0), offset(0), state(State::Idle), databuf() {};

  bool pending() const { return (state == State::ReadReq) || (state == State::WriteReq); }

  void start() override ;
  void done(TransferStatus status) override;

  void loadPlcProgram(void* buffer, uint_fast16_t length);
  void storePlcProgram(void* buffer, uint_fast16_t length);

  void finalize(bool success);

  friend class ManagePlcWidget;
};


class Bsp
{
public:
  enum class KeyState : uint_least8_t
  {
    ButtonNone = 0,
    Button1    = 1,
    Button2    = 2,
    Button3    = 3,
    Button4    = 4,
  };

  class SimpleTimer
  {
  private:
    uint_least32_t timeout_ticks;

  public:
    int32_t getTicksSinceTimeout() const;

    void startMilliseconds(unsigned long Timeout)
    {
      timeout_ticks = (nrfx_rtc_counter_get(&s_rtc1) + Timeout / 20) & 0xFFFFFF;
    }

    bool hasTimedOut() const {return getTicksSinceTimeout() >= 0; }
  };

private:
    static constexpr unsigned int pin_sda       =  9;
    static constexpr unsigned int pin_scl       = 10;

    static constexpr unsigned int pin_lcd_rst   = 13;

    static constexpr unsigned int pin_led_green = 15;
    static constexpr unsigned int pin_led_red   = 17;

    static constexpr unsigned int pin_vref_en   = 27;

    static constexpr unsigned int pin_dq[8]     = {7,8,24,23,20,18,11,12};

    static constexpr uint_least8_t twi_addr_eeprom = 0x50;

    static void init_led();

    void init_system();

    static void init_tick();
    static void init_i2c();
    static void init_rtc();
    void init_adc();

    static constexpr nrfx_pwm_t     s_pwm    = NRFX_PWM_INSTANCE(0);
    static constexpr nrfx_twim_t    s_twim   = NRFX_TWIM_INSTANCE(0);
    static constexpr nrfx_timer_t   s_timer1 = NRFX_TIMER_INSTANCE(1);
    static constexpr nrfx_rtc_t     s_rtc1   = NRFX_RTC_INSTANCE(1);

    static void reset(Lcd &lcd);

    void start_analog_calibration();
    void start_analog_conversion();

    static void nrf_fault_handler(uint32_t id, uint32_t pc, uint32_t info);
    static void event_twim(nrfx_twim_evt_t const * p_event, void *p_context);
    static void timer1_event(nrf_timer_event_t event_type,  void *p_context);
    static void adc_event(nrfx_saadc_evt_t const * p_event);
    static void rtc1_event(nrfx_rtc_int_type_t int_type);

    uint_least8_t softdevice_init_status;

    enum class AdcState : uint_least8_t
    {
      Idle        = 0,
      Calibrating = 1,
      Converting  = 2,
      Finished    = 3,
    };

    volatile AdcState adc_state;

    ::ecpp::KeyDebouncer<KeyState> key_state;
    SimpleTimer                    key_timer;

    SimpleTimer                    adc_timer;

  /** current owner of the twi unit */
  TwiTransferController*  twi_owner;

  /** resources for lcd controller */
  TwiLcdController        twi_controller_lcd;
  /** resources for eeprom controller */
  TwiEepromController     twi_controller_eeprom;

  static Bsp s_instance;
public:
  enum IrqPriority : uint32_t
  {
    IrqPriority_AppLow    = 6,
    IrqPriority_AppLowest = 7,
  };

  typedef ::ecpp::DateTime< ::ecpp::FixedCenturyDate<20>, ::ecpp::DefaultTime > ClockType;

  static constexpr Bsp & get_instance() {return s_instance;}

  void init();

  void set_led_blink_red();
  void set_led_green();
  void set_led_red();
  void set_led_off();

  void startTwiTransfer(TwiTransferController* ctrl);
  void finishTwiTransfer(TwiTransferController* ctrl);

  TwiEepromController & getEepromController() {return twi_controller_eeprom;}

  constexpr bool      is_softdevice_available() const { return softdevice_init_status == NRF_SUCCESS;}
  constexpr uint32_t  get_softdevice_init_status() const {return softdevice_init_status;}

  bool is_lcd_update_possible() const {return twi_controller_lcd.state != 0; }
  static void refresh_lcd();

  ClockType clock;
  Lcd       lcd;

  nrf_saadc_value_t adc_values[7] __attribute__((aligned(4)));

  constexpr KeyState get_key_state() const { return key_state.getKeyState(); }
  int_fast16_t getTicksSinceKeyChange() const {return key_timer.getTicksSinceTimeout();}

  void poll();

  template<typename T>
  void get_ai(unsigned int i, T & value);

  template<typename T>
  void get_asupply(T & value) {get_ai(4, value);}

  void set_dq(unsigned int i, uint_least8_t state)
  {
    if (i < 8)
    {
      if (state)
        nrf_gpio_pin_set(pin_dq[i]);
      else
        nrf_gpio_pin_clear(pin_dq[i]);
    }
  }

  bool get_dq(unsigned int i)
  {
    if (i < 8)
      return (0 < nrf_gpio_pin_out_read(pin_dq[i])) ;
    else
      return false;
  }

  friend class Lcd;
  friend class TwiTransferController;
  friend class TwiEepromController;
};


#endif