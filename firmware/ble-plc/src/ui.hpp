#ifndef UI_HPP_
#define UI_HPP_

#include "bsp.hpp"
#include "PlcProgram.hpp"

#include "ecpp/Datatypes.hpp"

class LcdWidget
{
public:
    virtual void draw();
    virtual void key_event(Bsp::KeyState key, int_fast16_t pressed);
    virtual void state_changed(void *p);
};

class IdleWidget : public LcdWidget
{
public:
  void draw() override;
  void key_event(Bsp::KeyState key, int_fast16_t pressed) override;
};

class BluetoothInfoWidget : public LcdWidget
{
public:
  void draw() override;
  void key_event(Bsp::KeyState key, int_fast16_t pressed) override;
};


class ScrollWidget : public LcdWidget
{
protected:
  const char* const     title;
  uint_least8_t         entries;
  uint_least8_t         position;
public:
  constexpr ScrollWidget(const char* title, uint_least8_t entries) : LcdWidget(), title(title), entries(entries), position(0) {}

  void draw();
  void key_event(Bsp::KeyState key, int_fast16_t pressed) override;

  virtual void draw_item(uint_least8_t idx);
  virtual void press_item(uint_least8_t idx);
};

class MenuScrollWidget : public ScrollWidget
{
private:
  const char* const *names;
public:
  MenuScrollWidget(const char *title, const char* const *names, uint_least8_t entries) : ScrollWidget(title, entries), names(names) {};
  void draw_item(uint_least8_t idx) override;
};

class LocalStatusScrollWidget : public ScrollWidget
{
public:
  LocalStatusScrollWidget() : ScrollWidget("  Status  ", 6) {};

  void draw_item(uint_least8_t idx) override;
  void state_changed(void *p) override;
};

class SetDateTimeWidget : public ScrollWidget
{
public:
  constexpr SetDateTimeWidget() : ScrollWidget("Datum/Zeit", 5) {}

  void draw_item(uint_least8_t idx) override;
  void key_event(Bsp::KeyState key, int_fast16_t pressed) override;
};

class ManagePlcWidget : public ScrollWidget
{
private:
  enum Hint : uint_least8_t
  {
    None,
    EEPSuccess,
    EEPFailure,
  };

  Hint hint = Hint::None;

  uint_least8_t last_progress = 0xff;
public:
  constexpr ManagePlcWidget() : ScrollWidget("Program", 3) {}

  void draw_item(uint_least8_t idx) override;
  void key_event(Bsp::KeyState key, int_fast16_t pressed) override;
  void state_changed(void *p) override;
};

class UiParameterInterface : public ::PlcApplication::PlcParameterInterface
{
public:
  constexpr UiParameterInterface() : manager_mem() {}

  virtual ::PlcApplication::VariableManager* createVariableManager(const ::PlcApplication::PlcVariableMetaBase &meta, const char* name, void *var_ptr);
private:
  /** memory to store a manager */
  ::ecpp::MemoryBuffer<ManagerTypes> manager_mem;

  template<typename MANAGER_TYPE>
  ::PlcApplication::VariableManager* newVariableManager(const ::PlcApplication::PlcVariableMetaBase &meta, const char* name, void *var_ptr)
  {
    if (sizeof(manager_mem) < sizeof(MANAGER_TYPE))
      return nullptr;

    return new (manager_mem.buf) MANAGER_TYPE(reinterpret_cast<const typename MANAGER_TYPE::MetaType &>(meta), name, var_ptr);
  }
};

class ManagePlcProgramParametersWidget : public ScrollWidget
{
public:
  ManagePlcProgramParametersWidget();

  void draw_item(uint_least8_t idx) override;
  void press_item(uint_least8_t idx) override;
  void state_changed(void *p) override;
  void key_event(Bsp::KeyState key, int_fast16_t pressed) override;
private:
  /* pointer to manager of the variable */
  ::PlcApplication::VariableManager* var_mgr_ptr;

  UiParameterInterface param_intf;
};


class ConfigurePlcProgramParameterWidget : public LcdWidget
{
private:
  ::PlcApplication::VariableManager                                                  & var_mgr;
  ::ecpp::MemoryBuffer<::UiParameterInterface::PlcParameterInterface::VariableTypes>   var_mem;

public:
  ConfigurePlcProgramParameterWidget(::PlcApplication::VariableManager & var_mgr);

  void draw() override ;
  void key_event(Bsp::KeyState key, int_fast16_t pressed) override;
  void state_changed(void *p) override;

  friend class ManagePlcProgramParametersWidget;
};

class Ui
{
public:
  static constexpr unsigned int widget_idle_timeout = 15000;

private:
    union AllWidgets
    {
        LcdWidget  widget;

        IdleWidget idle;

        BluetoothInfoWidget      bluetooth_info;

        MenuScrollWidget         menu;

        SetDateTimeWidget        setdatetime;

        LocalStatusScrollWidget  local_status;

        ManagePlcWidget                    manage_plc;

        ManagePlcProgramParametersWidget   manage_plc_parameters;

        ConfigurePlcProgramParameterWidget configure_plc_parameter;
    };

    static constexpr uint_least8_t stack_size         = 4;
    static constexpr uint_least8_t stack_element_size = sizeof(AllWidgets);

    uint_least8_t invalidated;
    uint_least8_t stack_depth;

    uint_least8_t bluetooth_state_changed;

    Bsp::SimpleTimer widget_idle_timer;
    Bsp::KeyState    last_key_state;

    unsigned int  widget_buffer[stack_size][(stack_element_size + sizeof(unsigned int) - 1) / sizeof(unsigned int)];

    constexpr LcdWidget * get_widget_ptr(uint_fast8_t idx)
    {
      return reinterpret_cast<LcdWidget*>(&widget_buffer[idx][0]);
    }

    constexpr LcdWidget* get_current_widget_ptr()
    {
      return get_widget_ptr(stack_depth);
    }

    constexpr LcdWidget & get_current_widget()
    {
      return *get_widget_ptr(stack_depth);
    }

    static Ui s_instance;
public:
    static constexpr Ui & get_instance() {return s_instance;}

    template<typename T, typename... ARGS>
    T * push_widget(ARGS & ... args)
    {
      T* ptr;

      if( stack_depth >=  stack_size)
        return nullptr;

      ptr = reinterpret_cast<T*>(get_widget_ptr(stack_depth+1));
      new ( ptr ) T(args...);
      stack_depth++;

      widget_idle_timer.startMilliseconds(widget_idle_timeout);
      invalidate();

      return ptr;
    }

    void pop_widget(LcdWidget * widget);

    void init();

    void draw(Lcd &lcd);
    void poll();
    void invalidate() {invalidated = true;}

    void invalidate(const LcdWidget *widget)
    {
      if (get_widget_ptr(stack_depth) == widget)
        invalidate();
    }

    void state_changed(void *p);

    friend class TwiLcdController;
};

#endif