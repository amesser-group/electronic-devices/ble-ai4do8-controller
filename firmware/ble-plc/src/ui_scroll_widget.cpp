#include <cstring>

#include "ecpp/String.hpp"
#include "ui.hpp"

using namespace ::std;
using namespace ::ecpp;

void
ScrollWidget::draw()
{
  auto & lcd  = Bsp::get_instance().lcd;
  char *row;

  lcd.getRowCommand(0).setTextCentered(title, 10);

  row = reinterpret_cast<char*>(lcd.getRow(3));
  strncpy(row,"X |<  >|OK", 10);
  String::convertToDecimal(row+4, 2, position);

  draw_item(position);
}

void ScrollWidget::key_event(Bsp::KeyState key, int_fast16_t pressed)
{
  if (pressed != 1)
    return;

  switch(key)
  {
  case Bsp::KeyState::Button1:
    Ui::get_instance().pop_widget(this);
    break;
  case Bsp::KeyState::Button2:
    if (position > 0)
      position--;
    else if (entries > 1)
      position = entries - 1;
    break;
  case Bsp::KeyState::Button3:
    if ((position + 1) < entries)
      position++;
    else
      position = 0;
    break;
  case Bsp::KeyState::Button4:
    press_item(position);
    break;
  case Bsp::KeyState::ButtonNone:
    break;
  }

  Ui::get_instance().invalidate(this);
}

void ScrollWidget::press_item(uint_least8_t)
{

}