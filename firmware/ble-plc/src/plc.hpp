#ifndef PLC_HPP_
#define PLC_HPP_

#include <cstdint>

#include "PlcProgram.hpp"
#include "PlcProgramImage.hpp"

class Plc
{
public:
  enum PlcFlags : uint_least8_t
  {
    ProgramStarted = 0x01,
    ProgramStopped = 0x02,
    TimeJumped     = 0x04
  };

  enum PlcState : uint_least8_t
  {
    Poweron,
    LoadingEEProm,
    Loaded,
    Stopped,
    Running,
    LoadError,
  };

  static constexpr Plc & get_instance() {return s_instance;}

  constexpr Plc() : state(PlcState::Poweron), state_req(PlcState::Poweron), flags(0), program(nullptr), ai(), asupply(), dq() {}

  PlcState getState() const {return state;}
  PlcState getRequestedState() const {return state_req;}
  void setRequestState(PlcState s) { state_req = s;}

  const ::PlcApplication::AnalogInput    & get_ai(unsigned int idx) const {return ai[idx];}

  const ::PlcApplication::AnalogInput    & get_asupply()            const {return asupply;}

  void init();
  void tick();

  void handleTimeChanged() { flags |= PlcFlags::TimeJumped; }

  bool initProgram();

  void loadFlashProgram();

  void loadEEPromProgram();
  void storeEEPromProgram();

  void update_inputs();
  void update_outputs();

  ::PlcApplication::VariableIdType    getParameterCount() const;
  ::PlcApplication::VariableManager*  createParameterManager(::PlcApplication::PlcParameterInterface &param_intf, ::PlcApplication::VariableIdType var_id);

  friend class PlcInterface;
  friend class TwiEepromController;

private:
  bool validatePointer(void *p) const;

  bool relocateProgram(PlcProgramImage &img);
  bool validateProgramPointer(const PlcProgramImage &img, void *p) const;

  bool validateProgram(const PlcProgramImage &img) const;

  static Plc s_instance;

  volatile PlcState state;

  PlcState state_req;

  uint_least8_t flags;

  ::PlcApplication::Program   *program;

  ::PlcApplication::AnalogInput   ai[4];

  ::PlcApplication::AnalogInput   asupply;

  ::PlcApplication::DigitalOutput dq[8];

};

class PlcInterface : public ::PlcApplication::PlcProgramInterface
{
private:
  Plc &plc;

  const ::PlcApplication::PlcDateTimeType date_time;

  ::PlcApplication::DigitalOutput dummy  {};
public:
  template<typename DATETIME>
  constexpr PlcInterface(Plc & plc, const DATETIME & dt) : plc(plc), date_time(dt) {};

  bool hasTimeJumped() override;

  const ::PlcApplication::PlcDateTimeType & getDateTime() override;

  ::PlcApplication::AnalogInput     asupply() override;
  ::PlcApplication::  AnalogInput     ai(unsigned int idx) override;
  ::PlcApplication::DigitalOutput & dq(unsigned int idx) override;
};

#endif