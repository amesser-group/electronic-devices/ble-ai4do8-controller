#include "PlcProgramImage.hpp"

#include "bsp.hpp"
#include "plc.hpp"
#include "ui.hpp"

#include <cstring>
#include <new>

extern "C"
{
  extern const uint8_t __plc_program_lma__;

  extern        uint8_t __plc_program_start__;
  extern        uint8_t __plc_program_end__;
}

Plc Plc::s_instance;

void
Plc::init()
{
  state_req = PlcState::Running;
}

bool
Plc::initProgram()
{
  PlcProgramImage & img = plc_program;
  const uintptr_t plc_mem_end   = reinterpret_cast<uintptr_t>(&__plc_program_end__);
  uintptr_t       plc_ram_start;

  if(!relocateProgram(img))
    return false;

  if (!validateProgram(img))
    return false;

  plc_ram_start = (reinterpret_cast<uintptr_t>(img.program_end_ptr) + 3) & 0xFFFFFFFC;

  if (nullptr == img.create_program_ptr)
    return false;

  program = img.create_program_ptr( reinterpret_cast<void*>(plc_ram_start), plc_mem_end - plc_ram_start);

  if(nullptr == program)
    return false;

  flags   = PlcFlags::TimeJumped;

  return true;
}

bool
Plc::relocateProgram(PlcProgramImage &img)
{
  Elf32_Rel *rel;
  Elf32_Rel *rel_end;
  uintptr_t        offset;
  uintptr_t        base;

  offset = reinterpret_cast<uintptr_t>(&img) - reinterpret_cast<uintptr_t>(img.self_ptr);

  /* no relocation required */
  if(0 == offset)
    return true;

  rel     = reinterpret_cast<Elf32_Rel*> (reinterpret_cast<uintptr_t>(img.rel_start_ptr) + offset);
  rel_end = reinterpret_cast<Elf32_Rel*> (reinterpret_cast<uintptr_t>(img.rel_end_ptr) + offset);

  if (! validatePointer(rel))
    return false;

  if (! validatePointer(rel_end))
    return false;

  base    = reinterpret_cast<uintptr_t>(&img);
  while(rel < rel_end)
  {
    uintptr_t* p;

    if (R_ARM_RELATIVE != ELF32_R_TYPE(rel->r_info))
      break;

    p  = reinterpret_cast<uintptr_t*>(base + rel->r_offset);
    *p += offset;

    rel++;
  }

  if(rel < rel_end)
  {
    /* invalidate image on error */
    img.create_program_ptr = nullptr;
    return false;
  }

  return true;
}

bool
Plc::validatePointer(void *p) const
{
  const uint8_t  *plc_mem_start = &__plc_program_start__;
  const uint8_t  *plc_mem_end   = &__plc_program_end__;

  if (p < plc_mem_start)
    return false;

  if (p >= plc_mem_end)
    return false;

  return true;
}

bool
Plc::validateProgramPointer(const PlcProgramImage &img, void *p) const
{
  if (p < (&img + 1))
    return false;

  if (p >= img.program_end_ptr)
    return false;

  return validatePointer(p);
}

bool
Plc::validateProgram(const PlcProgramImage &img) const
{
  if(img.self_ptr != &img)
    return false;

  if (!validateProgramPointer(img, reinterpret_cast<void*>(img.create_program_ptr)))
    return false;

  if (! validatePointer(img.program_end_ptr))
    return false;

  return true;
}


void
Plc::loadFlashProgram()
{
  const uintptr_t plc_mem_start = reinterpret_cast<uintptr_t>(&__plc_program_start__);
  const uintptr_t plc_mem_end   = reinterpret_cast<uintptr_t>(&__plc_program_end__);

  program = nullptr;

  /* relocate built in plc programm from flash to ram */
  memcpy(&__plc_program_start__, &__plc_program_lma__, plc_mem_end - plc_mem_start);

  state = PlcState::Loaded;
}

void
Plc::storeEEPromProgram()
{
  auto & storage = Bsp::get_instance().getEepromController();

  if(nullptr == program)
    return;

  /* for consistency, program must be stopped to be stored */
  if(state != PlcState::Stopped)
    return;

  storage.storePlcProgram(&__plc_program_start__, &__plc_program_end__ - &__plc_program_start__);
}

void
Plc::loadEEPromProgram()
{
  auto & storage = Bsp::get_instance().getEepromController();

  /* Program must be stopped for loading from eeprom  */
  if(state == PlcState::Running)
    return;

  program = nullptr;

  state = PlcState::LoadingEEProm;

  storage.loadPlcProgram(&__plc_program_start__, &__plc_program_end__ - &__plc_program_start__);
}



void
Plc::tick()
{
  auto const & bsp = Bsp::get_instance();
  auto s = state;

  switch(s)
  {
  case PlcState::Poweron:
    if(state_req == PlcState::Running)
      loadFlashProgram();
    break;
  case PlcState::LoadingEEProm:
    break;
  case PlcState::Loaded:
    if (initProgram())
    {
      state = PlcState::Stopped;
    }
    else
    {
      state     = PlcState::LoadError;
      state_req = PlcState::LoadError;
    }
    break;
  case PlcState::Stopped:
    if(state_req == PlcState::Running)
    {
      flags |= PlcFlags::ProgramStarted;
      state  = PlcState::Running;
    }
    break;
  case PlcState::Running:
    if(state_req == PlcState::Stopped)
    {
      flags |= PlcFlags::ProgramStopped;
      state  = PlcState::Stopped;
    }
    break;
  case PlcState::LoadError:
    break;
  }

  if ( (nullptr != program) &&
       (PlcState::Running == s) )
  {
    PlcInterface intf(*this, bsp.clock);
    program->tick(intf);

    flags = 0;
  }
  else
  {
    dq[0].value = 0;
    dq[1].value = 0;
    dq[2].value = 0;
    dq[3].value = 0;
    dq[4].value = 0;
    dq[5].value = 0;
    dq[6].value = 0;
    dq[7].value = 0;
  }

  update_outputs();
}

void Plc::update_inputs()
{
  auto & bsp = Bsp::get_instance();

  bsp.get_ai(0, ai[0].value);
  bsp.get_ai(1, ai[1].value);
  bsp.get_ai(2, ai[2].value);
  bsp.get_ai(3, ai[3].value);

  bsp.get_asupply(asupply.value);

  Ui::get_instance().state_changed(this);
}

void Plc::update_outputs()
{
  auto & bsp = Bsp::get_instance();
  int i;

  for(i = 0; i < 8; ++i)
    bsp.set_dq(i, dq[i].value);
}

::PlcApplication::VariableIdType
Plc::getParameterCount() const
{
  if(nullptr == program)
    return 0;

  return program->getVariableCount();
}

::PlcApplication::VariableManager*
Plc::createParameterManager(::PlcApplication::PlcParameterInterface &param_intf, ::PlcApplication::VariableIdType var_id)
{
  if(nullptr == program)
    return nullptr;

  return program->getVariableManager(param_intf, var_id);
}

const ::PlcApplication::PlcDateTimeType &
PlcInterface::getDateTime()
{
  return date_time;
}

::PlcApplication::AnalogInput
PlcInterface::asupply()
{
  return plc.asupply;
}

::PlcApplication::AnalogInput
PlcInterface::ai(unsigned int idx)
{
  switch(idx)
  {
  case 0:
  case 1:
  case 2:
  case 3:
    return plc.ai[idx];
    break;
  default:
    return {};
  }
}

::PlcApplication::DigitalOutput &
PlcInterface::dq(unsigned int idx)
{
  if(idx < 8)
    return plc.dq[idx];
  else
    return dummy;
}

bool
PlcInterface::hasTimeJumped()
{
  return 0 != (plc.flags & (Plc::PlcFlags::TimeJumped |
                            Plc::PlcFlags::ProgramStarted));
}