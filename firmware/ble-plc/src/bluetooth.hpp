#ifndef BLUETOOTH_HPP_
#define BLUETOOTH_HPP_
#define BLUETOOTH_HPP_

#include "ble.h"
#include "nrf_soc.h"

extern "C" void SD_EVT_IRQHandler();

class BluetoothStatusGuard;

class Bluetooth
{
public:
  struct SoftDeviceStatus
  {
    /** result code of last error */
    uint_least32_t last_error_rc;
    /** The program counter of the instruction that triggered the fault. */
    uint_least32_t fault_pc;
    /** Fault identifier. See @ref NRF_FAULT_IDS. */
    uint_least16_t fault_id;
  };

  struct Characteristic
  {
    ble_uuid_t            uuid;
    ble_gatt_char_props_t props;
    uint_least8_t         cccd;
  };


  struct CharacteristicState
  {
    ble_gatts_char_handles_t handles;
  };

private:
  enum class Unit : unsigned long
  {
    UNIT_1_25_MS = 1250,
    UNIT_10_MS   = 10000,
  };

  constexpr unsigned long MSec2Units(unsigned long long ms, Unit unit)
  {
    return ms * 1000ULL / static_cast<unsigned long>(unit);
  }

  /** array to store advertisment data, only one required per device */
  static uint8_t s_advertise_data[BLE_GAP_ADV_MAX_SIZE];

  static void nrf_fault_handler(uint32_t id, uint32_t pc, uint32_t info);

  uint32_t add_characteristic(uint8_t uuid_type, uint16_t service_handle, const Characteristic &c, CharacteristicState &state);


  void handle_ble_event(ble_evt_t *p_ev);
  void handle_sd_event(uint32_t evt_id);

  struct SoftDeviceStatus sd_status;

  uint_least8_t           ble_enabled;
  uint_least8_t           event_pending;

  uint8_t                 uart_uuid;
  uint16_t                uart_service_handle;

  CharacteristicState     uart_tx_characteristic;

  CharacteristicState     uart_rx_characteristic;

  static Bluetooth        s_instance;

  char                    uart_rx_chars[10];
public:
  static Bluetooth & get_instance() {return s_instance;}

  void init();
  void enable_uart();
  void advertise();
  void poll();

  constexpr SoftDeviceStatus & get_status() { return sd_status;}

  constexpr const char* getRxChars() const {return uart_rx_chars;}

  friend void SD_EVT_IRQHandler();
  friend class BluetoothStatusGuard;
};

class BluetoothStatusGuard
{
private:
  Bluetooth & blue;
public:
  constexpr BluetoothStatusGuard(Bluetooth& b, uint32_t app_ram_base) : blue(b), app_ram_base(app_ram_base), rc(NRF_SUCCESS), stage(0) {};
  ~BluetoothStatusGuard();

  uint32_t      app_ram_base;
  uint32_t      rc;
  uint_least8_t stage;
};

#endif
