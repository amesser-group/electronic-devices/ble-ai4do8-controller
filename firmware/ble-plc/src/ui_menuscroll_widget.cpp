#include <cstring>
#include "ui.hpp"

using namespace ::std;

void
MenuScrollWidget::draw_item(uint_least8_t idx)
{
  auto & lcd  = Bsp::get_instance().lcd;
  lcd.getRowCommand(1).setText(names[idx], 10);
}