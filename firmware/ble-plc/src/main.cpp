#include "bluetooth.hpp"
#include "bsp.hpp"
#include "plc.hpp"
#include "ui.hpp"

int main(int argc, char* argv[])
{
  auto & plc = Plc::get_instance();
  auto & bsp = Bsp::get_instance();

  Ui::get_instance().init();

  bsp.init();

  Bluetooth::get_instance().init();

  Bluetooth::get_instance().enable_uart();

  Bluetooth::get_instance().advertise();

  plc.init();

  while(true)
  {
    bsp.poll();

    Bluetooth::get_instance().poll();

    plc.tick();

    Ui::get_instance().poll();

    __WFE();
  }

  return 0;
}

extern "C" void _start(void);

void _start(void)
{
  main(0, nullptr);
}