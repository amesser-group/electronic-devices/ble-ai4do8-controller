#include <cstring>
#include "bluetooth.hpp"
#include "bsp.hpp"
#include "ui.hpp"
#include "ble.h"
#include "nrf_nvic.h"
#include "nrf_sdm.h"
#include "nrf_soc.h"

using namespace ::std;


/** used to reserve extra ram for softdevive according to configuration */
uint32_t s_softdevice_ram[1024 + 256 + 128 + 512] __attribute__((section(".ramsd")));

extern unsigned int __softdevice_ram_end;

Bluetooth Bluetooth::s_instance;

#define BLUETOOTH_CONNCFG_TAG (1)

static const char s_bluetooth_devicename[] = "AI4DO8";

void
Bluetooth::init()
{
  BluetoothStatusGuard guard(*this, reinterpret_cast<uint32_t>(&__softdevice_ram_end));

  ble_cfg_t ble_cfg;
  ble_gap_conn_params_t   gap_conn_params;
  ble_gap_conn_sec_mode_t sec_mode;

  guard.stage = 1;
  guard.rc = Bsp::get_instance().get_softdevice_init_status();

  if(guard.rc != NRF_SUCCESS)
    return;

  /* nrf_sdh.c: swi_intterrupt_priority_workaround */
  guard.stage ++;
  guard.rc = NRFX_IRQ_PRIORITY_SET(SD_EVT_IRQn, Bsp::IrqPriority_AppLow);

  if(guard.rc != NRF_SUCCESS)
    return;

  guard.stage ++;
  guard.rc = NRFX_IRQ_PRIORITY_SET(RADIO_NOTIFICATION_IRQn, Bsp::IrqPriority_AppLow);

  if(guard.rc != NRF_SUCCESS)
    return;

  guard.stage ++;
  guard.rc = NRFX_IRQ_ENABLE(SD_EVT_IRQn);

  if(guard.rc != NRF_SUCCESS)
    return;

  /* start nrf_sdh_ble_default_cfg_set() */

  memset(&ble_cfg, 0x00, sizeof(ble_cfg));
  ble_cfg.conn_cfg.conn_cfg_tag = BLUETOOTH_CONNCFG_TAG;
  ble_cfg.conn_cfg.params.gap_conn_cfg.conn_count   = 2;
  ble_cfg.conn_cfg.params.gap_conn_cfg.event_length = BLE_GAP_EVENT_LENGTH_DEFAULT;

  guard.stage ++;
  guard.rc = sd_ble_cfg_set(BLE_CONN_CFG_GAP, &ble_cfg, guard.app_ram_base);

  memset(&ble_cfg, 0x00, sizeof(ble_cfg));
  ble_cfg.gap_cfg.role_count_cfg.periph_role_count  = 1;
  ble_cfg.gap_cfg.role_count_cfg.central_role_count = 0; /* TODO, set if we're going to connect */
  ble_cfg.gap_cfg.role_count_cfg.central_sec_count  = 0;

  guard.stage ++;
  guard.rc = sd_ble_cfg_set(BLE_GAP_CFG_ROLE_COUNT, &ble_cfg, guard.app_ram_base);

  memset(&ble_cfg, 0x00, sizeof(ble_cfg));
  ble_cfg.conn_cfg.conn_cfg_tag = BLUETOOTH_CONNCFG_TAG;
  ble_cfg.conn_cfg.params.gatt_conn_cfg.att_mtu = 247; // NRF_SDH_BLE_GATT_MAX_MTU_SIZE 247

  guard.stage ++;
  guard.rc = sd_ble_cfg_set(BLE_CONN_CFG_GATT, &ble_cfg, guard.app_ram_base);

  memset(&ble_cfg, 0x00, sizeof(ble_cfg));
  ble_cfg.common_cfg.vs_uuid_cfg.vs_uuid_count = 1; /* number of uuids */

  guard.stage ++;
  guard.rc = sd_ble_cfg_set(BLE_COMMON_CFG_VS_UUID, &ble_cfg, guard.app_ram_base);

  memset(&ble_cfg, 0x00, sizeof(ble_cfg));
  ble_cfg.gatts_cfg.attr_tab_size.attr_tab_size = 1408; /* magic value from ble_app_uart/sdk_config.h */

  guard.stage ++;
  guard.rc = sd_ble_cfg_set(BLE_GATTS_CFG_ATTR_TAB_SIZE, &ble_cfg, guard.app_ram_base);

  memset(&ble_cfg, 0x00, sizeof(ble_cfg));
  ble_cfg.gatts_cfg.service_changed.service_changed = 0; /* magic value from ble_app_uart/sdk_config.h */

  guard.stage ++;
  guard.rc = sd_ble_cfg_set(BLE_GATTS_CFG_SERVICE_CHANGED, &ble_cfg, guard.app_ram_base);

  /* end nrf_sdh_ble_default_cfg_set() */

#if 0 /* not used by nordic example */
  memset(&ble_cfg, 0x00, sizeof(ble_cfg));
  ble_cfg.conn_cfg.conn_cfg_tag = BLUETOOTH_CONNCFG_TAG;
  ble_cfg.conn_cfg.params.gatts_conn_cfg.hvn_tx_queue_size = BLE_GATTS_HVN_TX_QUEUE_SIZE_DEFAULT;

  guard.stage ++;
  guard.rc = sd_ble_cfg_set(BLE_CONN_CFG_GATTS, &ble_cfg, guard.app_ram_base);
#endif

  guard.stage ++;
  guard.rc = sd_ble_enable(&(guard.app_ram_base));

  if(guard.rc != NRF_SUCCESS)
    return;

  ble_enabled = true;

  /* The following is taken from ble_app_uart/main.c */

  /* gap_params_init */
  BLE_GAP_CONN_SEC_MODE_SET_OPEN(&sec_mode);

  guard.stage ++;
  guard.rc = sd_ble_gap_device_name_set(&sec_mode, (uint8_t*)s_bluetooth_devicename,
                               sizeof(s_bluetooth_devicename) - 1);

  if(guard.rc != NRF_SUCCESS)
    return;

  memset(&gap_conn_params, 0, sizeof(gap_conn_params));

  gap_conn_params.min_conn_interval = MSec2Units(20, Unit::UNIT_1_25_MS);
  gap_conn_params.max_conn_interval = MSec2Units(75, Unit::UNIT_1_25_MS);
  gap_conn_params.slave_latency     = 0;
  gap_conn_params.conn_sup_timeout  = MSec2Units(4000, Unit::UNIT_10_MS);

  guard.stage ++;
  guard.rc = sd_ble_gap_ppcp_set(&gap_conn_params);
}

static const ble_uuid128_t s_bluetooth_uart_uuid = {
  {0x9E, 0xCA, 0xDC, 0x24, 0x0E, 0xE5, 0xA9, 0xE0, 0x93, 0xF3, 0xA3, 0xB5, 0x00, 0x00, 0x40, 0x6E}
};

static const Bluetooth::Characteristic s_bluetooth_uart_tx_characteristic =
{
  { 0x0003, BLE_UUID_TYPE_VENDOR_BEGIN},
  { 0, /**< Broadcasting of the value permitted. */
    0, /**< Reading the value permitted. */
    0, /**< Writing the value with Write Command permitted. */
    0, /**< Writing the value with Write Request permitted. */
    1, /**< Notification of the value permitted. */
    0, /**< Indications of the value permitted. */
    0, /**< Writing the value with Signed Write Command permitted. */
  },
  1,
};

static const Bluetooth::Characteristic s_bluetooth_uart_rx_characteristic =
{
  { 0x0002, BLE_UUID_TYPE_VENDOR_BEGIN},
  { 0, /**< Broadcasting of the value permitted. */
    0, /**< Reading the value permitted. */
    1, /**< Writing the value with Write Command permitted. */
    1, /**< Writing the value with Write Request permitted. */
    0, /**< Notification of the value permitted. */
    0, /**< Indications of the value permitted. */
    0, /**< Writing the value with Signed Write Command permitted. */
  },
  0,
};


uint32_t
Bluetooth::add_characteristic(uint8_t uuid_type, uint16_t service_handle, const Characteristic &c, CharacteristicState &state)
{
  ble_gatts_char_md_t char_md;
  ble_gatts_attr_md_t attr_md;
  ble_gatts_attr_md_t cccd_md;
  ble_gatts_attr_t    attr_char_value;
  ble_uuid_t          uuid;

  uint32_t rc;

  uuid = c.uuid;

  if (uuid.type == BLE_UUID_TYPE_VENDOR_BEGIN)
    uuid.type = uuid_type;

  memset(&char_md, 0, sizeof(char_md));

  char_md.char_props = c.props;

  if (c.cccd)
  {
    memset(&cccd_md, 0, sizeof(cccd_md));
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&cccd_md.read_perm);
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&cccd_md.write_perm);
    cccd_md.vloc = BLE_GATTS_VLOC_STACK;
    char_md.p_cccd_md = &cccd_md;
  }

  memset(&attr_md, 0, sizeof(attr_md));

  BLE_GAP_CONN_SEC_MODE_SET_OPEN(&attr_md.read_perm);
  BLE_GAP_CONN_SEC_MODE_SET_OPEN(&attr_md.write_perm);

  attr_md.vloc    = BLE_GATTS_VLOC_STACK;
  attr_md.rd_auth = 0;
  attr_md.wr_auth = 0;
  attr_md.vlen    = 1;

  memset(&attr_char_value, 0, sizeof(attr_char_value));

  attr_char_value.p_uuid    = &uuid;
  attr_char_value.p_attr_md = &attr_md;
  attr_char_value.init_len  = sizeof(uint8_t);
  attr_char_value.init_offs = 0;
  attr_char_value.max_len   = (BLE_GATT_ATT_MTU_DEFAULT - 3);

  rc = sd_ble_gatts_characteristic_add(service_handle,
                                       &char_md,
                                       &attr_char_value,
                                       &state.handles);

  return rc;
}




void Bluetooth::enable_uart()
{
  BluetoothStatusGuard guard(*this, reinterpret_cast<uint32_t>(&__softdevice_ram_end));
  ble_uuid_t uuid;

  if (not ble_enabled)
    return;

  /* register base uart uuid */
  guard.stage ++;
  guard.rc = sd_ble_uuid_vs_add(&s_bluetooth_uart_uuid, &uart_uuid);

  if(guard.rc != NRF_SUCCESS)
    return;

  /* register service uuid */
  uuid.type = uart_uuid;
  uuid.uuid = 0x0001;

  guard.stage ++;
  guard.rc = sd_ble_gatts_service_add(BLE_GATTS_SRVC_TYPE_PRIMARY, &uuid, &uart_service_handle);

  if(guard.rc != NRF_SUCCESS)
    return;

  guard.stage ++;
  guard.rc = add_characteristic(uart_uuid, uart_service_handle, s_bluetooth_uart_rx_characteristic, uart_rx_characteristic);

  if(guard.rc != NRF_SUCCESS)
    return;

  guard.stage ++;
  guard.rc = add_characteristic(uart_uuid, uart_service_handle, s_bluetooth_uart_tx_characteristic, uart_tx_characteristic);

  if(guard.rc != NRF_SUCCESS)
    return;

}

uint8_t Bluetooth::s_advertise_data[BLE_GAP_ADV_MAX_SIZE];

#define BLE_ADV_LENGTH_FIELD_SIZE   1
#define BLE_ADV_AD_TYPE_FIELD_SIZE  1
#define BLE_AD_TYPE_FLAGS_DATA_SIZE 1


void
Bluetooth::advertise()
{
  BluetoothStatusGuard guard(*this, reinterpret_cast<uint32_t>(&__softdevice_ram_end));
  ble_gap_conn_sec_mode_t sec_mode;
  ble_gap_adv_params_t adv_params;
  uint8_t pos, size_pos, encoded_size;
  ble_uuid_t uuid;

  if (not ble_enabled)
    return;

  pos = 0;
  BLE_GAP_CONN_SEC_MODE_SET_OPEN(&sec_mode);

  guard.stage ++;
  guard.rc = sd_ble_gap_device_name_set(&sec_mode, (uint8_t*)s_bluetooth_devicename,
                               sizeof(s_bluetooth_devicename) - 1);

  if(guard.rc != NRF_SUCCESS)
    return;

  s_advertise_data[pos] = 1 + sizeof(s_bluetooth_devicename) - 1;
  pos++;

  s_advertise_data[pos] = BLE_GAP_AD_TYPE_COMPLETE_LOCAL_NAME;
  pos++;

  memcpy(&(s_advertise_data[pos]), s_bluetooth_devicename, sizeof(s_bluetooth_devicename) - 1);
  pos += sizeof(s_bluetooth_devicename) - 1;

  size_pos = pos;
  pos ++;

  s_advertise_data[pos] = BLE_GAP_AD_TYPE_128BIT_SERVICE_UUID_COMPLETE;
  pos++;

  guard.stage ++;

  /* for all service uuid */
  uuid.type = uart_uuid;
  uuid.uuid = 0x0001;

  guard.rc = sd_ble_uuid_encode(&uuid, &encoded_size, NULL);

  if(guard.rc != NRF_SUCCESS)
    return;

  if ( (pos + encoded_size) > sizeof(s_advertise_data))
    return;

  guard.rc = sd_ble_uuid_encode(&uuid, &encoded_size, &(s_advertise_data[pos]));

  if(guard.rc != NRF_SUCCESS)
    return;

  pos += encoded_size;
  /* end for service uuid */

  s_advertise_data[size_pos] = (pos - size_pos) - 1;


  guard.stage ++;
  guard.rc = sd_ble_gap_adv_data_set(s_advertise_data, pos, NULL, 0);

  if(guard.rc != NRF_SUCCESS)
    return;

  memset(&adv_params, 0, sizeof(adv_params));

  adv_params.type     = BLE_GAP_ADV_TYPE_ADV_IND;
  adv_params.fp       = BLE_GAP_ADV_FP_ANY;
  adv_params.timeout  = 300;
  adv_params.interval = 160;

  guard.stage ++;
  guard.rc = sd_ble_gap_adv_start(&adv_params,BLUETOOTH_CONNCFG_TAG);
}

BluetoothStatusGuard::~BluetoothStatusGuard()
{
  if (rc == NRF_SUCCESS)
    return;

  if (blue.sd_status.last_error_rc == NRF_SUCCESS)
    blue.sd_status.last_error_rc = rc;

  Ui::get_instance().state_changed(&blue);
}

void Bluetooth::handle_sd_event(uint32_t evt_id)
{

}

void Bluetooth::handle_ble_event(ble_evt_t *p_ev)
{
  switch(p_ev->header.evt_id)
  {
  case BLE_GAP_EVT_CONNECTED:
      // m_adv_in_progress = false;

      ble_gap_conn_params_t conn_params;
      (void)sd_ble_gap_ppcp_get(&conn_params);
      (void)sd_ble_gap_conn_param_update(p_ev->evt.gap_evt.conn_handle, &conn_params);

      Ui::get_instance().state_changed(this);
      break;

  case BLE_GAP_EVT_DISCONNECTED:
      Ui::get_instance().state_changed(this);
      /* ble_uart_advertise */
      break;

  case BLE_GAP_EVT_TIMEOUT:
      /* Advertise timeout */
      advertise();
      break;

  case BLE_GATTS_EVT_HVC:
     /* uart us <- pc */
      break;

  case BLE_GATTS_EVT_WRITE:
      uint16_t  handle   = p_ev->evt.gatts_evt.params.write.handle;
      uint16_t  data_len = p_ev->evt.gatts_evt.params.write.len;
      uint8_t * p_data   = &p_ev->evt.gatts_evt.params.write.data[0];

      if(handle == uart_rx_characteristic.handles.value_handle)
      { /* received data on uart */
        memset(uart_rx_chars, ' ', 10);

        if (data_len > 10)
          data_len = 10;

        memcpy(uart_rx_chars, p_data, data_len);

        Ui::get_instance().state_changed(this);
      }

      /* uart us -> pc confirmation  */
      /* m_cccd_enabled = true */
      break;

  }
}

static uint8_t m_ble_evt_buf[sizeof(ble_evt_t) + (BLE_GATT_ATT_MTU_DEFAULT)] __attribute__ ((aligned (4)));

void Bluetooth::poll()
{
  uint32_t evt_id;

  if(!event_pending)
    return;

  event_pending = false;

  while(NRF_ERROR_NOT_FOUND != sd_evt_get(&evt_id))
  {
    handle_sd_event(evt_id);
  }

  if (ble_enabled)
  {
    while (1)
    {
        uint16_t evt_len  = sizeof(m_ble_evt_buf);
        uint32_t err_code = sd_ble_evt_get(m_ble_evt_buf, &evt_len);

        if (err_code != NRF_SUCCESS) {
            // Possible error conditions:
            //  * NRF_ERROR_NOT_FOUND: no events left, break
            //  * NRF_ERROR_DATA_SIZE: retry with a bigger data buffer
            //    (currently not handled, TODO)
            //  * NRF_ERROR_INVALID_ADDR: pointer is not aligned, should
            //    not happen.
            // In all cases, it's best to simply stop now.
            if (err_code == NRF_ERROR_DATA_SIZE) {
            }
            break;
        }
        handle_ble_event((ble_evt_t *)m_ble_evt_buf);
    }
  }

}

#ifndef SD_EVT_IRQHandler
#error Handler not defined
#endif

void SD_EVT_IRQHandler()
{
  auto & blue = Bluetooth::get_instance();

  blue.event_pending = true;
}