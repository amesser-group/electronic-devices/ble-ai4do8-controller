#include <cstring>
#include "ecpp/Datatypes.hpp"
#include "ecpp/Math/Fixedpoint.hpp"
#include "bsp.hpp"
#include "plc.hpp"
#include "PlcProgram.hpp"
#include "ui.hpp"

#include "nrf_sdm.h"
#include "nrf_soc.h"

using namespace std;
using namespace ecpp;

/* this is needed for softdevice */
nrf_nvic_state_t nrf_nvic_state;

/* led settings */
static constexpr uint_fast16_t s_led_top_value    = 250*125 - 1;
static constexpr uint_fast16_t s_led_invert_value = 0x8000;
static constexpr uint_fast16_t s_led_off_value    = s_led_invert_value | 0;
static constexpr uint_fast16_t s_led_pulse_value  = s_led_invert_value | s_led_top_value / 2;
static constexpr uint_fast16_t s_led_on_value     = s_led_invert_value | s_led_top_value;

/* must be in ram due to easy-dma */
static nrf_pwm_values_grouped_t s_led_sequences[] =
{
    /* green led 500 ms */
    { s_led_off_value, s_led_on_value},
    { s_led_off_value, s_led_on_value},
    /* red led 500ms */
    { s_led_on_value, s_led_off_value},
    { s_led_on_value, s_led_off_value},
    /* both leds off 500ms */
    { s_led_off_value, s_led_off_value},
    { s_led_off_value, s_led_off_value},
};

static const nrf_pwm_sequence_t s_led_sequence_blink_red =
{
  {(nrf_pwm_values_common_t*)(&s_led_sequences[2])},
  8,
  0,
  0
};

static const nrf_pwm_sequence_t s_led_sequence_red =
{
  {(nrf_pwm_values_common_t*)(&s_led_sequences[2])},
  4,
  0,
  0
};

static const nrf_pwm_sequence_t s_led_sequence_green =
{
  {(nrf_pwm_values_common_t*)(&s_led_sequences[0])},
  4,
  0,
  0
};

static const nrf_pwm_sequence_t s_led_sequence_off =
{
  {(nrf_pwm_values_common_t*)(&s_led_sequences[4])},
  4,
  0,
  0
};

Bsp Bsp::s_instance;

void
Bsp::init()
{
    init_led();

    init_system();

    init_tick();

    init_rtc();

    init_i2c();

    init_adc();

    lcd.init();
}

void
Bsp::init_system()
{
  nrf_clock_lf_cfg_t clock_cfg;
  uint32_t mask;

  /** used for delays */
  nrfx_systick_init();

  /* enable nordic soft device with */
  clock_cfg.source       = NRF_CLOCK_LF_SRC_RC;
  clock_cfg.rc_ctiv      = 16;
  clock_cfg.rc_temp_ctiv = 2;
  clock_cfg.accuracy     = NRF_CLOCK_LF_ACCURACY_500_PPM;

  softdevice_init_status = sd_softdevice_enable(&clock_cfg, nrf_fault_handler);

  if(NRF_SUCCESS == softdevice_init_status)
  {
    sd_power_dcdc_mode_set(NRF_POWER_DCDC_ENABLE);
  }

  /* configure dq port to be an output */
  mask = (1 << pin_dq[0]) |
         (1 << pin_dq[2]) |
         (1 << pin_dq[1]) |
         (1 << pin_dq[3]) |
         (1 << pin_dq[4]) |
         (1 << pin_dq[5]) |
         (1 << pin_dq[6]) |
         (1 << pin_dq[7]);

  nrf_gpio_port_out_clear(NRF_P0, mask);
  nrf_gpio_port_dir_output_set(NRF_P0, mask);
}

void
Bsp::init_led()
{
    nrfx_pwm_config_t config = {};

    config.output_pins[0] = pin_led_red | NRFX_PWM_PIN_INVERTED;
    config.output_pins[1] = NRFX_PWM_PIN_NOT_USED;
    config.output_pins[2] = pin_led_green | NRFX_PWM_PIN_INVERTED;
    config.output_pins[3] = NRFX_PWM_PIN_NOT_USED;

    config.base_clock     = NRF_PWM_CLK_125kHz;
    config.count_mode     = NRF_PWM_MODE_UP;
    config.top_value      = s_led_top_value;
    config.load_mode      = NRF_PWM_LOAD_GROUPED;
    config.step_mode      = NRF_PWM_STEP_AUTO;

    nrfx_pwm_init(&s_pwm, &config, NULL);
    nrfx_pwm_simple_playback(&s_pwm, &s_led_sequence_blink_red, 1, NRFX_PWM_FLAG_LOOP);
}


void
Bsp::set_led_blink_red()
{
  nrfx_pwm_sequence_update(&s_pwm, 0, &s_led_sequence_blink_red);
  nrfx_pwm_sequence_update(&s_pwm, 1, &s_led_sequence_blink_red);
}

void Bsp::set_led_green()
{
  nrfx_pwm_sequence_update(&s_pwm, 0, &s_led_sequence_green);
  nrfx_pwm_sequence_update(&s_pwm, 1, &s_led_sequence_green);
}

void Bsp::set_led_red()
{
  nrfx_pwm_sequence_update(&s_pwm, 0, &s_led_sequence_red);
  nrfx_pwm_sequence_update(&s_pwm, 1, &s_led_sequence_red);
}

void Bsp::set_led_off()
{
  nrfx_pwm_sequence_update(&s_pwm, 0, &s_led_sequence_off);
  nrfx_pwm_sequence_update(&s_pwm, 1, &s_led_sequence_off);
}

void
Bsp::init_rtc()
{
  nrfx_timer_config_t config;

  config.frequency = NRF_TIMER_FREQ_31250Hz;
  config.mode      = NRF_TIMER_MODE_TIMER;
  config.bit_width = NRF_TIMER_BIT_WIDTH_16;
  config.interrupt_priority = 7;
  config.p_context = &(get_instance().clock);

  nrfx_timer_init(&s_timer1, &config, Bsp::timer1_event);

/* to speed up time for development
 * #define TIMEWARP 1 */
#ifdef TIMEWARP
  nrfx_timer_extended_compare(&s_timer1, NRF_TIMER_CC_CHANNEL0, 3000,
                               NRF_TIMER_SHORT_COMPARE0_CLEAR_MASK, true);
#else
  nrfx_timer_extended_compare(&s_timer1, NRF_TIMER_CC_CHANNEL0, 31249,
                               NRF_TIMER_SHORT_COMPARE0_CLEAR_MASK, true);
#endif

  nrfx_timer_enable(&s_timer1);
}

void
Bsp::start_analog_calibration()
{
  adc_state = AdcState::Calibrating;
  nrfx_saadc_calibrate_offset();
}

void
Bsp::start_analog_conversion()
{
  adc_state = AdcState::Converting;

  /* setup buffer to receive adc data */
  nrfx_saadc_buffer_convert(adc_values, ElementCount(adc_values));

  /* start sampling */
  nrfx_saadc_sample();
}

void
Bsp::rtc1_event(nrfx_rtc_int_type_t int_type)
{
  Bsp & bsp = Bsp::get_instance();

  if(AdcState::Idle == bsp.adc_state)
  {
    if (bsp.adc_timer.hasTimedOut())
      bsp.start_analog_calibration();
    else
      bsp.start_analog_conversion();
  }
}

void
Bsp::init_tick()
{
  nrfx_rtc_config_t config;

  /* setup 20ms tick */
  config.prescaler = RTC_FREQ_TO_PRESCALER(50);
  config.interrupt_priority = IrqPriority_AppLowest;
  config.tick_latency = 0;
  config.reliable = true;

  nrfx_rtc_init(&s_rtc1, &config, rtc1_event);

  nrfx_rtc_enable(&s_rtc1);

  nrfx_rtc_tick_enable(&s_rtc1, true);
}

static constexpr uint_least8_t s_i2c_lcd_addr = 0x3C;


static volatile uint_least8_t s_twim_transfer_pending = 0;

void Bsp::nrf_fault_handler(uint32_t id, uint32_t pc, uint32_t info)
{
  //auto & self = get_instance();
}


void
Bsp::event_twim(nrfx_twim_evt_t const * p_event, void *p_context)
{
  auto & bsp = Bsp::get_instance();

  if(bsp.twi_owner == nullptr)
    return;

  switch(p_event->type)
  {
  case NRFX_TWIM_EVT_DONE:
    bsp.twi_owner->done(TwiTransferController::TransferStatus::Sucessfull);
    break;
  case NRFX_TWIM_EVT_ADDRESS_NACK:
    bsp.twi_owner->done(TwiTransferController::TransferStatus::AddressNak);
    break;
  case NRFX_TWIM_EVT_DATA_NACK:
    bsp.twi_owner->done(TwiTransferController::TransferStatus::DataNak);
    break;
  }
}

void Bsp::timer1_event(nrf_timer_event_t event_type,  void *p_context)
{
  ClockType & clock = * reinterpret_cast<ClockType*>(p_context);

  if( NRF_TIMER_EVENT_COMPARE0 == event_type)
  {
    clock.add({0,0,1});
    Ui::get_instance().state_changed(&clock);
  }
}

void
Bsp::adc_event(nrfx_saadc_evt_t const * p_event)
{
  Bsp & bsp = Bsp::get_instance();

  switch(p_event->type)
  {
  case NRFX_SAADC_EVT_DONE:
    if (AdcState::Converting == bsp.adc_state )
      bsp.adc_state = AdcState::Finished;
    break;
  case NRFX_SAADC_EVT_CALIBRATEDONE:
    bsp.adc_timer.startMilliseconds(10000);

    if (AdcState::Calibrating == bsp.adc_state )
      bsp.start_analog_conversion();
    break;
  }
}

void
Bsp::reset(Lcd &lcd)
{
   nrf_gpio_pin_set(pin_lcd_rst);
   nrf_gpio_cfg_output(pin_lcd_rst);

   nrfx_systick_delay_ms(5);

   nrf_gpio_pin_clear(pin_lcd_rst);
   nrfx_systick_delay_ms(10);

   nrf_gpio_pin_set(pin_lcd_rst);
   nrfx_systick_delay_ms(1);
}

void
Bsp::init_i2c()
{
    nrfx_twim_config_t config = {};

    config.scl                = pin_scl;
    config.sda                = pin_sda;
    config.frequency          = NRF_TWIM_FREQ_100K;
    config.hold_bus_uninit    = true;
    config.interrupt_priority = 7;

    nrfx_twim_init(&s_twim, &config, event_twim, NULL);

    /* keep i2c permanently enabled */
    nrfx_twim_enable(&s_twim);
}

void
Bsp::init_adc()
{
  nrfx_saadc_config_t config = {};
  nrf_saadc_channel_config_t channel_config;

  config.resolution          = NRF_SAADC_RESOLUTION_14BIT;
  config.oversample          = NRF_SAADC_OVERSAMPLE_DISABLED;
  config.interrupt_priority  = 7;
  config.low_power_mode = false;

  nrfx_saadc_init(&config, adc_event);

  /* 0-3V input scale for AI0-3*/
  channel_config = NRFX_SAADC_DEFAULT_CHANNEL_CONFIG_SE(NRF_SAADC_INPUT_AIN6);
  channel_config.reference = NRF_SAADC_REFERENCE_INTERNAL;
  channel_config.gain      = NRF_SAADC_GAIN1_5;
  channel_config.acq_time  = NRF_SAADC_ACQTIME_40US;

  nrfx_saadc_channel_init(0, &channel_config);

  channel_config.pin_p = NRF_SAADC_INPUT_AIN7;
  nrfx_saadc_channel_init(1, &channel_config);

  channel_config.pin_p = NRF_SAADC_INPUT_AIN0;
  nrfx_saadc_channel_init(2, &channel_config);

  channel_config.pin_p = NRF_SAADC_INPUT_AIN3;
  nrfx_saadc_channel_init(3, &channel_config);

  /* 0-3V input scale for vdd*/
  channel_config.pin_p = NRF_SAADC_INPUT_AIN1;
  nrfx_saadc_channel_init(4, &channel_config);

  /* 0-3V input scale for ref 2.5V*/
  channel_config.pin_p = NRF_SAADC_INPUT_AIN5;
  nrfx_saadc_channel_init(5, &channel_config);

  /* 0-66% input scale for key */
  channel_config = NRFX_SAADC_DEFAULT_CHANNEL_CONFIG_SE(NRF_SAADC_INPUT_AIN4);
  channel_config.reference = NRF_SAADC_REFERENCE_VDD4;
  channel_config.gain      = NRF_SAADC_GAIN1_3;
  channel_config.acq_time  = NRF_SAADC_ACQTIME_40US;
  nrfx_saadc_channel_init(6, &channel_config);

  /* enable voltage reference */
  nrf_gpio_cfg_output(pin_vref_en);
  nrf_gpio_pin_set(pin_vref_en);

}

void
Bsp::startTwiTransfer(TwiTransferController *ctrl)
{
  if(nullptr != twi_owner)
    return;

  twi_owner = ctrl;
  twi_owner->start();
}

void
Bsp::finishTwiTransfer(TwiTransferController *ctrl)
{
  if(ctrl != twi_owner)
    return;

  twi_owner = nullptr;

  Ui::get_instance().state_changed(ctrl);
}


void
TwiTransferController::startWrite(uint_least8_t addr, const void* data, size_t datalen, bool send_stop)
{
  auto & bsp = Bsp::get_instance();

  if(bsp.twi_owner != this)
    return;

  nrfx_twim_tx(&bsp.s_twim, addr, reinterpret_cast<const uint8_t*>(data), datalen, !send_stop);
}

  /** invoke to request a twi read */
void
TwiTransferController::startRead(uint_least8_t addr, void* data, size_t datalen)
{
  auto & bsp = Bsp::get_instance();

  if(bsp.twi_owner != this)
    return;

  nrfx_twim_rx(&bsp.s_twim, addr, reinterpret_cast<uint8_t*>(data), datalen);
}


void
TwiTransferController::startWriteRead(uint_least8_t addr, void* write_data, size_t write_len, void* read_data, size_t read_len)
{
  nrfx_twim_xfer_desc_t xfer = NRFX_TWIM_XFER_DESC_TXRX(addr,
                                                        reinterpret_cast<uint8_t*>(write_data), write_len,
                                                        reinterpret_cast<uint8_t*>(read_data), read_len);
  auto & bsp = Bsp::get_instance();

  if(bsp.twi_owner != this)
    return;

  nrfx_twim_xfer(&(bsp.s_twim), &xfer, 0);
}

static const uint8_t s_lcd_init_sequence[] =
{
#if 0 /* using ROMa requires several adaptions */
    Lcd::cmd_control_command | Lcd::cmd_control_continue,
    0x3A, /* function set: RE := 1  */
    Lcd::cmd_control_command | Lcd::cmd_control_continue,
    0x72, /* rom selection */
    Lcd::cmd_control_data | Lcd::cmd_control_continue,
    0x00, /* ROM A */
#endif
    Lcd::cmd_control_command, /* command bytes will follows */
    0x3A, /* function set: RE := 1  */
    0x09, /* 4 lines display */
    0x05, /* top view */
    0x1E, /* BS1=1 */
    0x39, /* function set  RE := 0, IS := 1*/
    0x1B, /* BS0=1 -> Bias1/6 */
    0x6E, /* Divider on, value */
    0x56, /* booster on, set contrast */
    0x7A, /* contrast */
    0x38, /* function set RE := 0, IS := 0*/
    0x0F, /* display on */
};

void Lcd::init()
{
  auto & bsp = Bsp::get_instance();

  bsp.reset(*this);

  /* sanity, should never be true */
  while(nullptr != bsp.twi_owner);

  bsp.twi_owner = &(bsp.twi_controller_lcd);
  bsp.twi_controller_lcd.init();
}

void Lcd::clear()
{
  uint_fast8_t i;

  memset(display_data, 0x20, sizeof(display_data));

  for(i = 0; i < rows; ++ i)
  {
      display_data[i].control[0] = cmd_control_continue | cmd_control_command;
      display_data[i].control[1] = makeLocateCommand(0,i);
      display_data[i].control[2] = cmd_control_data;
  }

  extra_data.data[0] = cmd_control_command;
  extra_data.data[1] = makeLocateCommand(10,4);
}

/** Implements mapping for RAM C for SSD1803 */
void Lcd::remapCharacters()
{
  uint_fast8_t i,j;

  for(i = 0; i < rows; ++ i)
  {
    for(j = 0; j < sizeof(display_data[i].text); ++j)
    {
      switch(display_data[i].text[j])
      {
        case '\0': display_data[i].text[j] = ' ';  break;
        case '�' : display_data[i].text[j] = 0x84; break;
        case '�' : display_data[i].text[j] = 0x94; break;
        case '�' : display_data[i].text[j] = 0x81; break;
        case '�' : display_data[i].text[j] = 0x8E; break;
        case '�' : display_data[i].text[j] = 0x99; break;
        case '�' : display_data[i].text[j] = 0x9A; break;
      }
    }
  }
}


bool
TwiLcdController::pending() const
{
  return Ui::get_instance().invalidated != 0;
}

void
TwiLcdController::start()
{
  auto & lcd = Bsp::get_instance().lcd;

  lcd.clear();
  Ui::get_instance().draw(lcd);

  lcd.remapCharacters();

  state = 0;
  done(TransferStatus::Sucessfull);
}

void
TwiLcdController::done(TransferStatus status)
{
  auto & bsp = Bsp::get_instance();
  auto & lcd = bsp.lcd;

  if(TransferStatus::Sucessfull != status)
    bsp.finishTwiTransfer(this);

  if(state < 4)
  {
    auto & cmd = lcd.getRowCommand(state);

    state++;
    startWrite(s_i2c_lcd_addr, cmd.getData(), cmd.getLength());
  }
  else if(state == 4)
  {
    auto & cmd = lcd.getExtraCommand();

    state++;
    startWrite(s_i2c_lcd_addr, cmd.getData(), cmd.getLength());
  }
  else
  {
    bsp.finishTwiTransfer(this);
  }
}

void
TwiLcdController::init()
{ /* write init sequence to display */
  auto & lcd = Bsp::get_instance().lcd;

  state = 5;

  memcpy(lcd.display_data, s_lcd_init_sequence, sizeof(s_lcd_init_sequence));
  startWrite(s_i2c_lcd_addr, lcd.display_data, sizeof(s_lcd_init_sequence));
}


void Bsp::poll()
{
  if (AdcState::Finished == adc_state)
  {
    nrf_saadc_value_t val;

    /* process keyboard adc value*/
    val = adc_values[6];

    /* clamp to positive value */
    if (val < 0)
      val = 0;

    /* calculation matches characteristics of button resistor matrix */
    key_state.poll(static_cast<KeyState>((val + 300) / 3000), key_timer);

    /* update inputs in plc memory */
    Plc::get_instance().update_inputs();

    adc_state = AdcState::Idle;
  }

  if(nullptr == twi_owner)
  {
    if(twi_controller_lcd.pending())
      startTwiTransfer(&twi_controller_lcd);
    else if(twi_controller_eeprom.pending())
      startTwiTransfer(&twi_controller_eeprom);
  }

  //Ui::get_instance().state_changed(&clock);
}


template<>
void Bsp::get_ai(unsigned int idx,::ecpp::FixedPoint<int_least16_t, 1000> & value)
{
  int_fast32_t a, b;

  /* analog_ch(idx) / analog_ch(5) * 2.5 * 115/15 */

  a  = adc_values[idx];
  a *= 2500 * 23;

  b  = adc_values[5] * 3;

  if (b > 0)
    value.set_raw(a/b);
  else
    value.set_raw(::ecpp::TypeProperties<int_least16_t>::Max);

}

int32_t
Bsp::SimpleTimer::getTicksSinceTimeout() const
{
  int32_t val;

  val = nrfx_rtc_counter_get(&(Bsp::s_rtc1)) - timeout_ticks;

  /* make 24 bit sign extension */
  if (val & 0xFF800000)
    val |= 0xFF800000;

  return val;
}

void
Lcd::RowCommand::setText(const char *s, uint_fast8_t slen)
{
  constexpr uint_fast8_t cols = sizeof(text);
/* get real text length */
  slen = strnlen(s, slen);

  memset(text,' ', cols);
  memcpy(text, s, slen);
}

void
Lcd::RowCommand::setTextCentered(const char *s, uint_fast8_t slen)
{
  constexpr uint_fast8_t cols = sizeof(text);

  /* get real text length */
  slen = strnlen(s, slen);

  memset(text,' ', cols);

  if (slen < cols)
    memcpy(&(text[(cols-slen)/2]), s, slen);
  else
    memcpy(text, s, slen);
}


uint_fast8_t
Lcd::RowCommand::calcProgress(uint_fast32_t done, uint_fast32_t amount)
{
  uint_fast32_t val;

  val = done * 50 / amount;

  if(val > 50)
    val = 50;

  return val;
}

/* print progrss bar */
uint_fast8_t
Lcd::RowCommand::makeProgress(uint_fast32_t done, uint_fast32_t amount)
{
  uint_fast8_t val, ret;
  uint_fast8_t i;

  val = ret = calcProgress(done, amount);

  if(val > 50)
    val = 50;

  for(i=0; i < 10; ++i)
  {
    if(val >= 5)
      text[i] = 0xd0;
    else if (val > 0)
      text[i] = 0xd4 - val;
    else
      break;

    val -= 5;
  }

  return ret;
}

void
TwiEepromController::finalize(bool success)
{
  auto & bsp = Bsp::get_instance();
  State s;

  s = state;
  state = State::Idle;

  bsp.finishTwiTransfer(this);

  if(State::ReadReq == s)
  {
    auto & plc = Plc::get_instance();

    if(Plc::PlcState::LoadingEEProm == plc.state)
    {
      if(success)
        plc.state = Plc::PlcState::Loaded;
      else
        plc.state = Plc::PlcState::LoadError;
    }

    Ui::get_instance().state_changed(this);
  }
}

void
TwiEepromController::start()
{
  auto & bsp = Bsp::get_instance();
  uint_fast16_t ofs, len;

  ofs = offset;
  len = length - offset;

  databuf[0] = (ofs >> 8) & 0xFF;
  databuf[1] = (ofs >> 0) & 0xFF;

  if(len == 0)
  {
    finalize(true);
    return;
  }
  else if (State::ReadReq == state)
  {
    state = State::ReadPend;

    if (len > 0x3FF)
      len = 0x3FF;

    /* advance offset */
    offset = ofs + len;

    startWriteRead(bsp.twi_addr_eeprom, databuf, 2, buffer + ofs, len);
  }
  else if(State::WriteReq == state)
  {
    state = State::WritePend;

    /* limitation of eeprom */
    if (len > 32)
      len = 32;

    if (ofs % 32)
      len = 1;

    memcpy(&databuf[2], buffer+ofs, len);
    /* advance offset */
    offset = ofs + len;

    startWrite(bsp.twi_addr_eeprom, databuf, 2 + len);
  }
}

void
TwiEepromController::done(TransferStatus status)
{
  if(TransferStatus::Sucessfull != status)
  {
    /* reset offset */
    offset = ((uint_fast16_t)databuf[0] << 8) |
             ((uint_fast16_t)databuf[1] << 0);

    if( (State::WritePend == state) &&
        (TransferStatus::AddressNak == status))
    { /* eeprom busy, retry if this is not the first write request */

      if(offset != 0)
        status = TransferStatus::Sucessfull;
    }

    if(TransferStatus::Sucessfull != status)
    { /* error */
      finalize(false);
      return;
    }
  }

  switch(state)
  {
  case State::Idle:
    break;
  case State::ReadReq:
    break;
  case State::ReadPend:
    state = State::ReadReq;
    break;
  case State::WriteReq:
    break;
  case State::WritePend:
    state = State::WriteReq;
    break;
  }

  /* release ownerchip of twi */
  Bsp::get_instance().finishTwiTransfer(this);
  Ui::get_instance().state_changed(this);
}

void
TwiEepromController::loadPlcProgram(void* buf, uint_fast16_t len)
{
  if (State::Idle != state)
    return;

  buffer = reinterpret_cast<uint8_t*>(buf);
  length = len;
  offset = 0;

  state = State::ReadReq;
}

void
TwiEepromController::storePlcProgram(void* buf, uint_fast16_t len)
{
  if (State::Idle != state)
    return;

  buffer = reinterpret_cast<uint8_t*>(buf);
  length = len;
  offset = 0;

  state = State::WriteReq;
}
