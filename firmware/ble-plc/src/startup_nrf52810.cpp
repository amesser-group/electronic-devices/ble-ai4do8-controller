#include "system_nrf52810.hpp"

extern "C" void _start(void);
extern "C" void Default_Handler(void);
extern "C" void Reset_Handler(void);

#define DECLARE_DEFAULT_HANDLER(x) extern "C" void x (void) __attribute__ ((weak, alias("Default_Handler")))

DECLARE_DEFAULT_HANDLER(NMI_Handler);
DECLARE_DEFAULT_HANDLER(HardFault_Handler);
DECLARE_DEFAULT_HANDLER(MemoryManagement_Handler);
DECLARE_DEFAULT_HANDLER(BusFault_Handler);
DECLARE_DEFAULT_HANDLER(UsageFault_Handler);
DECLARE_DEFAULT_HANDLER(SVC_Handler);
DECLARE_DEFAULT_HANDLER(DebugMon_Handler);
DECLARE_DEFAULT_HANDLER(PendSV_Handler);
DECLARE_DEFAULT_HANDLER(SysTick_Handler);

/* External Interrupts */
DECLARE_DEFAULT_HANDLER(POWER_CLOCK_IRQHandler);
DECLARE_DEFAULT_HANDLER(RADIO_IRQHandler);
DECLARE_DEFAULT_HANDLER(UARTE0_IRQHandler);
DECLARE_DEFAULT_HANDLER(TWIM0_TWIS0_IRQHandler);
DECLARE_DEFAULT_HANDLER(SPIM0_SPIS0_IRQHandler);
DECLARE_DEFAULT_HANDLER(GPIOTE_IRQHandler);
DECLARE_DEFAULT_HANDLER(SAADC_IRQHandler);
DECLARE_DEFAULT_HANDLER(TIMER0_IRQHandler);
DECLARE_DEFAULT_HANDLER(TIMER1_IRQHandler);
DECLARE_DEFAULT_HANDLER(TIMER2_IRQHandler);
DECLARE_DEFAULT_HANDLER(RTC0_IRQHandler);
DECLARE_DEFAULT_HANDLER(TEMP_IRQHandler);
DECLARE_DEFAULT_HANDLER(RNG_IRQHandler);
DECLARE_DEFAULT_HANDLER(ECB_IRQHandler);
DECLARE_DEFAULT_HANDLER(CCM_AAR_IRQHandler);
DECLARE_DEFAULT_HANDLER(WDT_IRQHandler);
DECLARE_DEFAULT_HANDLER(RTC1_IRQHandler);
DECLARE_DEFAULT_HANDLER(QDEC_IRQHandler);
DECLARE_DEFAULT_HANDLER(COMP_IRQHandler);
DECLARE_DEFAULT_HANDLER(SWI0_EGU0_IRQHandler);
DECLARE_DEFAULT_HANDLER(SWI1_EGU1_IRQHandler);
DECLARE_DEFAULT_HANDLER(SWI2_IRQHandler);
DECLARE_DEFAULT_HANDLER(SWI3_IRQHandler);
DECLARE_DEFAULT_HANDLER(SWI4_IRQHandler);
DECLARE_DEFAULT_HANDLER(SWI5_IRQHandler);
DECLARE_DEFAULT_HANDLER(PWM0_IRQHandler);
DECLARE_DEFAULT_HANDLER(PDM_IRQHandler);

void Default_Handler(void)
{
    while (1);
}

/* symbol marking the source address in flash were initialized data
 * coes from */
extern unsigned int __data_start_lma__;

/* symbols marking start and end of data section in ram */
extern unsigned int __data_start__;
extern unsigned int __data_end__;

/* symbols marking start and end of bss section in ram */
extern unsigned int __bss_start__;
extern unsigned int __bss_end__;

typedef void (init_func_t)();

extern init_func_t* __init_array_start;
extern init_func_t* __init_array_end;

static void callConstructors()
{
    // Start and end points of the constructor list,
    // defined by the linker script.
    extern void (*__init_array_start)();
    extern void (*__init_array_end)();

    // Call each function in the list.
    // We have to take the address of the symbols, as __init_array_start *is*
    // the first function pointer, not the address of it.
    for (void (**p)() = &__init_array_start; p < &__init_array_end; ++p) {
        (*p)();
    }
}




/** Reset handler of system */
void Reset_Handler(void)
{
  unsigned int *pSrc, *pDst;
  init_func_t** init_func;

  /* relocate data section */
  for(pDst = &__data_start__, pSrc = &__data_start_lma__; pDst < &__data_end__; ++pDst, ++pSrc)
    *pDst = *pSrc;

  /* clear bss section */
  for(pDst = &__bss_start__; pDst < &__bss_end__; ++pDst)
    *pDst = 0;

  /* invoke initialization functions */
  for( init_func = &__init_array_start;
       init_func < &__init_array_end;
       ++init_func)
  {
    (*init_func)();
  }

   SystemInit();
   _start();
}

typedef void (isr_func_t)(void);

/* symbols marking the end of the stack */
extern unsigned int __stack_end__;

const isr_func_t* __isr_vector[] __attribute__ ((section(".isr_vector"),used)) =
{
    (isr_func_t*)&__stack_end__,
    Reset_Handler,
    NMI_Handler,
    HardFault_Handler,
    MemoryManagement_Handler,
    BusFault_Handler,
    UsageFault_Handler,
    nullptr,
    nullptr,
    nullptr,
    nullptr,
    SVC_Handler,
    DebugMon_Handler,
    nullptr,
    PendSV_Handler,
    SysTick_Handler,

    /* External Interrupts */
    POWER_CLOCK_IRQHandler,
    RADIO_IRQHandler,
    UARTE0_IRQHandler,
    TWIM0_TWIS0_IRQHandler,
    SPIM0_SPIS0_IRQHandler,
    nullptr,
    GPIOTE_IRQHandler,
    SAADC_IRQHandler,
    TIMER0_IRQHandler,
    TIMER1_IRQHandler,
    TIMER2_IRQHandler,
    RTC0_IRQHandler,
    TEMP_IRQHandler,
    RNG_IRQHandler,
    ECB_IRQHandler,
    CCM_AAR_IRQHandler,
    WDT_IRQHandler,
    RTC1_IRQHandler,
    QDEC_IRQHandler,
    COMP_IRQHandler,
    SWI0_EGU0_IRQHandler,
    SWI1_EGU1_IRQHandler,
    SWI2_IRQHandler,
    SWI3_IRQHandler,
    SWI4_IRQHandler,
    SWI5_IRQHandler,
    nullptr,
    nullptr,
    PWM0_IRQHandler,
    PDM_IRQHandler,
};
