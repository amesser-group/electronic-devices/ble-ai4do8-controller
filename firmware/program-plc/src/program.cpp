#include "PlcProgram.hpp"
#include "PlcProgramImage.hpp"

#include <new>

class TestProgram : public PlcProgram
{
private:
  PlcTimeType   last_update    {};

  uint_least8_t last_channel = 0;

  uint_least8_t speed        = 1;
public:
  constexpr TestProgram() : PlcProgram() {}

  void tick(PlcProgramInterface &intf) override;

  uint_fast8_t          getParameterCount(const PlcParameterInterface &intf) const override;

  PlcParameterManager * createParameterManager(const PlcParameterInterface &intf, uint_fast8_t idx, void* mem, size_t memlen) override;
};


void
TestProgram::tick(PlcProgramInterface &intf)
{
  if (intf.getDateTime().getTime() == last_update)
    return;

  last_update = intf.getDateTime();

  if(0 == (last_update.getSecond() % speed))
  {
    intf.dq(last_channel).value = false;

    if(last_channel < 7)
      last_channel++;
    else
      last_channel=0;

    intf.dq(last_channel).value = true;
  }
}

uint_fast8_t
TestProgram::getParameterCount(const PlcParameterInterface &intf) const
{
  return 1;
}

PlcParameterManager *
TestProgram::createParameterManager(const PlcParameterInterface &intf, uint_fast8_t idx, void* mem, size_t memlen)
{
  switch(idx)
  {
  case 0:
    return intf.createSimpleParameterManager(mem, memlen, "Geschw", speed, 1, 10);
  default:
    return nullptr;
  }
}



PlcProgram*
create_program(void *p_mem, unsigned int mem_len)
{
  if (mem_len < sizeof(TestProgram))
    return nullptr;

  return new (p_mem) TestProgram();
}

