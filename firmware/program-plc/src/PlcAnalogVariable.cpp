#include "PlcProgram.hpp"

using namespace PlcApplication;

void AnalogVariableManager::formatValue(const void* val, char* buf, size_t buflen) const
{
  const VariableType * val_ptr = castVoidPtr(val);

  if (buflen > 6)
    buflen = 6;

  if (buflen < 1)
    return;

  ::ecpp::String::formatSigned(buf, buflen - 1, val_ptr->get_raw());
  ::ecpp::String::formatFrac(buf, buflen, 3);
}

void AnalogVariableManager::copyValue(void* dst, const void* src) const
{
  VariableType* dst_ptr = castVoidPtr(dst);
  const VariableType *src_ptr = castVoidPtr(src);
  *dst_ptr = *src_ptr;
}

void AnalogVariableManager::increaseValue(void* val) const
{
  const auto & meta = getMeta<MetaType>();
  VariableType* val_ptr = castVoidPtr(val);

  if ( *val_ptr < meta.max)
    (*val_ptr) += 0.1;
}

void AnalogVariableManager::decreaseValue(void* val) const
{
  const auto & meta = getMeta<MetaType>();
  VariableType* val_ptr = castVoidPtr(val);

  if ( *val_ptr > meta.min)
    (*val_ptr) -= 0.1;
}
