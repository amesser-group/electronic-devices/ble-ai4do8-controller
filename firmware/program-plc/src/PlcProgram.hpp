
#ifndef PLCPROGRAM_HPP_
#define PLCPROGRAM_HPP_

#include "ecpp/Math/Fixedpoint.hpp"
#include "ecpp/Time.hpp"
#include "ecpp/String.hpp"

namespace PlcApplication
{
  class AnalogInput
  {
  public:
    typedef ::ecpp::FixedPoint<int_least16_t, 1000> AnalogValue;

    constexpr AnalogInput() : value(0) {}

    template<typename T>
    constexpr AnalogInput(const T init) : value(init) {}

    AnalogValue value;
  };

  class DigitalOutput
  {
  public:
    typedef uint_least8_t DigitalValue;

    constexpr DigitalOutput() : value(0) {}

    DigitalValue value;
  };

  typedef ::ecpp::DefaultTime PlcTimeType;
  typedef ::ecpp::TimeDelta   PlcTimeDeltaType;

  typedef ::ecpp::DefaultDate PlcDateType;
  typedef ::ecpp::DateDelta   PlcDateDeltaType;
  typedef ::ecpp::DateTime<PlcDateType, PlcTimeType> PlcDateTimeType;

  class PlcProgramInterface
  {
  public:
    constexpr PlcProgramInterface() {};

    /** Check if time has jumped since last call (e.g. Time set be user) */
    virtual bool hasTimeJumped();
    virtual const PlcDateTimeType & getDateTime();

    virtual AnalogInput     asupply();
    virtual AnalogInput     ai(unsigned int idx);
    virtual DigitalOutput & dq(unsigned int idx);
  };

  typedef uint_least8_t  VariableTypeIdType;
  typedef uint_least16_t VariableIdType;

  template<typename T>
  struct PlcTypeId {};

  template<typename T>
  struct PlcEnumerationTypeId {};


  template<> struct PlcTypeId<uint_least8_t>
  {
    static constexpr VariableTypeIdType type_id = 1;
  };

  template<> struct PlcEnumerationTypeId<uint_least8_t>
  {
    static constexpr VariableTypeIdType type_id = 2;
  };

  template<> struct PlcTypeId<PlcDateType>                { static constexpr VariableTypeIdType type_id = 20;};
  template<> struct PlcTypeId<PlcTimeType>                { static constexpr VariableTypeIdType type_id = 21;};
  template<> struct PlcTypeId<AnalogInput::AnalogValue>   { static constexpr VariableTypeIdType type_id = 22;};

  template<typename T>
  struct VariableMetaBaseHelper
  {
    static constexpr T dummy {};
  };

  /** Container to hold meta information about a variable */
  struct PlcVariableMetaBase
  {
    const VariableTypeIdType type_id;
    const uint_least8_t      size;

    constexpr PlcVariableMetaBase(VariableTypeIdType type_id, uint_least8_t size) : type_id(type_id), size(size) {}

    template<typename VAR_TYPE>
    constexpr PlcVariableMetaBase(VAR_TYPE dummy) : type_id(PlcTypeId<VAR_TYPE>::type_id), size(sizeof(VAR_TYPE)) {}
  };


  template<typename T>
  struct PrimitiveVariableMeta : public PlcVariableMetaBase
  {
    typedef T VariableType;

    const VariableType min;
    const VariableType max;

    constexpr PrimitiveVariableMeta(T min, T max, VariableTypeIdType type_id = PlcTypeId<T>::type_id) : PlcVariableMetaBase(type_id, sizeof(T)), min(min), max(max) {}
  };

  template<typename T>
  struct EnumerationVariableMeta : public PrimitiveVariableMeta<T>
  {
    const char** names;

    constexpr EnumerationVariableMeta(T min, T max, const char** names) : PrimitiveVariableMeta<T>(min,max, PlcEnumerationTypeId<T>::type_id), names(names) {}
  };

  struct AnalogVariableMeta : public PlcVariableMetaBase
  {
    typedef AnalogInput::AnalogValue VariableType;

    const VariableType min;
    const VariableType max;

    template<typename INIT_TYPE>
    constexpr AnalogVariableMeta(INIT_TYPE min, INIT_TYPE max) : PlcVariableMetaBase(VariableMetaBaseHelper<VariableType>::dummy), min(min), max(max) {}
  };

  struct DateVariableMeta : public PlcVariableMetaBase
  {
    typedef PlcDateType VariableType;

    constexpr DateVariableMeta() : PlcVariableMetaBase(VariableMetaBaseHelper<VariableType>::dummy) {}
  };


  /** Proxy to handle a variable */
  class VariableManager
  {
  protected:
    /** meta information about variable */
    const PlcVariableMetaBase & meta;
    /** name of the variable */
    const char*                 name;
    /** pointer to memory of the variable */
    void*                       var_ptr;

    template<typename T>
    constexpr const T & getMeta() const {return reinterpret_cast <const T&>(meta); }
  public:
    constexpr VariableManager(const PlcVariableMetaBase &meta, const char* name, void *var_ptr) : meta(meta), name(name), var_ptr(var_ptr) {};

    virtual void formatValue(const void* val, char* buf, size_t buflen) const;
    virtual void copyValue(void* dst, const void* src) const;
    virtual void increaseValue(void* val) const;
    virtual void decreaseValue(void* val) const;

    constexpr const char*   getName()    const {return name;}
    constexpr const void*   getVarPtr()  const {return var_ptr;}
    constexpr uint_least8_t getVarSize() const {return meta.size;}

    void                    setVar(const void* val) { copyValue(var_ptr, val);}
  };

  template<typename T>
  class IntegerVariableManager : public VariableManager
  {
  public:
    typedef PrimitiveVariableMeta<T>         MetaType;
    typedef typename MetaType::VariableType  VariableType;

    constexpr IntegerVariableManager(const MetaType &meta, const char* name, void *var_ptr) : VariableManager(meta,name,var_ptr) {};

    void formatValue(const void* val, char* buf, size_t buflen) const override;
    void copyValue(void* dst, const void* src) const override;
    void increaseValue(void* val) const override;
    void decreaseValue(void* val) const override;

    constexpr       VariableType * castVoidPtr(void* p) const { return reinterpret_cast<VariableType*>(p); }
    constexpr const VariableType * castVoidPtr(const void* p) const { return reinterpret_cast<const VariableType*>(p); }
  };

  template<typename T>
  void IntegerVariableManager<T>::formatValue(const void* val, char* buf, size_t buflen) const
  {
    const VariableType* val_ptr = castVoidPtr(val);
    ::ecpp::String::formatUnsigned(buf, buflen, *val_ptr);
  }

  template<typename T>
  void IntegerVariableManager<T>::copyValue(void* dst, const void* src) const
  {
    VariableType* dst_ptr = castVoidPtr(dst);
    const VariableType *src_ptr = castVoidPtr(src);
    *dst_ptr = *src_ptr;
  }

  template<typename T>
  void IntegerVariableManager<T>::increaseValue(void* val) const
  {
    const auto & meta = getMeta<MetaType>();
    VariableType* val_ptr = castVoidPtr(val);

    if ( *val_ptr < meta.max)
      ++(*val_ptr);
  }

  template<typename T>
  void IntegerVariableManager<T>::decreaseValue(void* val) const
  {
    const auto & meta = getMeta<MetaType>();
    VariableType* val_ptr = castVoidPtr(val);

    if ( *val_ptr > meta.min)
      --(*val_ptr);
  }

  template<typename T>
  class EnumerationVariableManager : public IntegerVariableManager<T>
  {
  public:
    typedef EnumerationVariableMeta<T>         MetaType;

    constexpr EnumerationVariableManager(const MetaType &meta, const char* name, void *var_ptr) : IntegerVariableManager<T>(meta,name,var_ptr) {};

    void formatValue(const void* val, char* buf, size_t buflen) const override;
  };

  template<typename T>
  void EnumerationVariableManager<T>::formatValue(const void* val, char* buf, size_t buflen) const
  {
    const auto & meta = VariableManager::getMeta<MetaType>();
    const typename IntegerVariableManager<T>::VariableType* val_ptr = IntegerVariableManager<T>::castVoidPtr(val);
    strncpy(buf, meta.names[*val_ptr],buflen);
  }

  class AnalogVariableManager : public VariableManager
  {
  public:
    typedef AnalogVariableMeta               MetaType;
    typedef typename MetaType::VariableType  VariableType;

    constexpr AnalogVariableManager(const MetaType &meta, const char* name, void *var_ptr) : VariableManager(meta,name,var_ptr) {};

    void formatValue(const void* val, char* buf, size_t buflen) const override;
    void copyValue(void* dst, const void* src) const override;
    void increaseValue(void* val) const override;
    void decreaseValue(void* val) const override;

    constexpr       VariableType * castVoidPtr(void* p) const { return reinterpret_cast<VariableType*>(p); }
    constexpr const VariableType * castVoidPtr(const void* p) const { return reinterpret_cast<const VariableType*>(p); }
  };

  class DateVariableManager : public VariableManager
  {
  public:
    typedef DateVariableMeta                 MetaType;
    typedef typename MetaType::VariableType  VariableType;

    constexpr DateVariableManager(const MetaType &meta, const char* name, void *var_ptr) : VariableManager(meta,name,var_ptr) {};

    void formatValue(const void* val, char* buf, size_t buflen) const override;
    void copyValue(void* dst, const void* src) const override;
    void increaseValue(void* val) const override;
    void decreaseValue(void* val) const override;

    constexpr       VariableType * castVoidPtr(void* p) const { return reinterpret_cast<VariableType*>(p); }
    constexpr const VariableType * castVoidPtr(const void* p) const { return reinterpret_cast<const VariableType*>(p); }
  };


  class PlcParameterInterface
  {
  public:
    /* union of known manager types */
    typedef union
    {
      ::PlcApplication::VariableManager                  common;
      ::PlcApplication::IntegerVariableManager<uint8_t>  uint8;
      ::PlcApplication::AnalogVariableManager            analog;
      ::PlcApplication::DateVariableManager              date;
    } ManagerTypes;

    /** union of known variable types */
    typedef union
    {
      uint8_t                  uint8;
      AnalogInput::AnalogValue analog;
      PlcDateType              date;
    } VariableTypes;

    /* create a variable manager for given variable */
    virtual VariableManager* createVariableManager(const PlcVariableMetaBase &meta, const char* name, void *var_ptr);

    template<typename META_TYPE>
    VariableManager* createVariableManager(const META_TYPE &meta, const char *name, typename META_TYPE::VariableType & var)
    {return createVariableManager(meta,name,&var);}
  };


  class Program
  {
  public:
    constexpr Program() {};

    virtual void tick(PlcProgramInterface &intf);

    /** get number of plc variables assigned with this program */
    virtual VariableIdType      getVariableCount() const;

    virtual VariableManager*    getVariableManager(PlcParameterInterface & param_intf, VariableIdType var_id);
  };
}

typedef PlcApplication::Program* (create_program_fn_t)(void *p_mem, unsigned int mem_len);

extern "C" create_program_fn_t create_program;


#endif