#ifndef PLCPGRAM_IMAGE_HPP__
#define PLCPGRAM_IMAGE_HPP__
#include "PlcProgram.hpp"

#include <cstdint>
#include <cstdio>
#include "elf.h"


struct PlcProgramImage
{
  struct PlcProgramImage    *self_ptr;

  PlcApplication::Program* (*create_program_ptr)(void *p_mem, unsigned int mem_len);

  unsigned int              *program_end_ptr;

  Elf32_Rel                 *rel_start_ptr;

  Elf32_Rel                 *rel_end_ptr;
};

extern "C" PlcProgramImage plc_program;

PlcApplication::Program *create_program(void *p_mem, unsigned int mem_len);

#endif
