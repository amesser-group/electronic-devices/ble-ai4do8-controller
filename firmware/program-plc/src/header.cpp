#include "PlcProgram.hpp"
#include "PlcProgramImage.hpp"

extern "C"
{
  extern unsigned int   __program_end__;
  extern Elf32_Rel      __rel_start__;
  extern Elf32_Rel      __rel_end__;

  PlcProgramImage plc_program __attribute__((section(".plc_program"), visibility("default"))) =
  {
    &plc_program,
    &create_program,
    &__program_end__,
    &__rel_start__,
    &__rel_end__,
  };
}
