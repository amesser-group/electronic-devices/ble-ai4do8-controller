#include "PlcProgram.hpp"

using namespace PlcApplication;

void DateVariableManager::formatValue(const void* val, char* buf, size_t buflen) const
{
  const VariableType * val_ptr = castVoidPtr(val);

  if (buflen < 10)
    return;

  if (buflen > 10)
    buf[10] = '\0';

  ::ecpp::formatDate(*val_ptr, *reinterpret_cast<char (*) [10]>(buf));
}

void DateVariableManager::copyValue(void* dst, const void* src) const
{
  VariableType* dst_ptr = castVoidPtr(dst);
  const VariableType *src_ptr = castVoidPtr(src);
  *dst_ptr = *src_ptr;
}

void DateVariableManager::increaseValue(void* val) const
{
}

void DateVariableManager::decreaseValue(void* val) const
{
}
