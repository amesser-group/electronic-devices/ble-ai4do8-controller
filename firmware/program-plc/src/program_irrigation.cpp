#include "PlcProgram.hpp"
#include "PlcProgramImage.hpp"

#include <new>

using namespace ::PlcApplication;

typedef uint_least8_t IrrigationDurationType;
typedef uint_least8_t IrrigationIntervalType;
typedef uint_least8_t JobNumberType;

struct IrrigationJob
{
  PlcDateTimeType          last;
};

class IrrigationParameters
{
public:
  constexpr IrrigationParameters() {}

  IrrigationDurationType    max_duration_per_day = 45;

  IrrigationDurationType    job_duration[4]       = {15,15,15,15};

  IrrigationIntervalType    job_interval[4]       = {2, 2, 2, 2};

  PlcTimeType               daily_start_time        {4,0,0};

  AnalogInput::AnalogValue  battery_full_detected   {13.2};
};

class IrrigationProgram : public Program
{
public:
  constexpr IrrigationProgram() {}

  void tick(PlcProgramInterface &intf) override;

  virtual VariableIdType      getVariableCount() const;

  virtual VariableManager*    getVariableManager(PlcParameterInterface & param_intf, VariableIdType var_id);

private:
  enum OperationMode : uint_least8_t
  {
    Auto = 0,
    ManualOff,
    Manual0,
    Manual1,
    Manual2,
    Manual3
  };

  enum class SchedulerState : uint_least8_t
  {
    WaitStartTime,
    ScheduleNextJob,
    WaitJobFinished,
  };

  enum class IrrigationManagerState : uint_least8_t
  {
    Idle,
    Activate,
    Starting,
    Run,
    Overrun,
    Shutdown,
  };

  static IrrigationParameters parameters;

  /** Four irrigation areas in garden */
  PlcDateType          last_job_exec[4]             = {};

  /* time when last update was made to algorithm */
  PlcTimeType      last_update                        {};

  PlcTimeType            irrigation_timer                {};

  PlcTimeType            irrigation_ts                   {};

  SchedulerState         state                      = SchedulerState::WaitStartTime;

  IrrigationManagerState irr_mgr_state              = IrrigationManagerState::Idle;

  OperationMode          opmode                     = OperationMode::Auto;

  JobNumberType          requested_job              = 0xFF;

  JobNumberType          active_job                 = 0xFF;


  IrrigationDurationType irrigation_duration_budget = 0;

  JobNumberType    getNextJobNumber(const PlcDateType &date) const;

  void             pollIrrigationManager(PlcProgramInterface &intf, const PlcDateTimeType & current_time);
};

IrrigationParameters IrrigationProgram::parameters;

void
IrrigationProgram::tick(PlcProgramInterface &intf)
{
  auto & current_time = intf.getDateTime();
  JobNumberType sched_job;

  if (current_time.getTime() == last_update)
    return;

  /* Detect start event if required */
  if (state == SchedulerState::WaitStartTime)
  {
    bool start_irrigation;

    /* detect daily start time reached */
    start_irrigation = (current_time.getTime() >= parameters.daily_start_time) &&
                       (last_update            <  parameters.daily_start_time);

    /* plc time was manually changed, ignore false positives therefore */
    if(intf.hasTimeJumped())
      start_irrigation = false;

    if(start_irrigation)
      state = SchedulerState::ScheduleNextJob;
  }

  last_update = current_time.getTime();

  switch(state)
  {
  default:
  case SchedulerState::WaitStartTime:
    sched_job = 0xff;
    break;
  case SchedulerState::ScheduleNextJob:
    sched_job = getNextJobNumber(current_time.getDate());

    if(opmode != OperationMode::Auto)
    {
      state = SchedulerState::WaitStartTime;
    }
    else if(sched_job < 4)
    {
      if( (IrrigationManagerState::Run == irr_mgr_state) &&
          (active_job == sched_job) )
      {
        state = SchedulerState::WaitJobFinished;
      }
    }
    else
    {
      state = SchedulerState::WaitStartTime;
    }
    break;
  case SchedulerState::WaitJobFinished:
    sched_job = requested_job;

    if(opmode != OperationMode::Auto)
      state = SchedulerState::ScheduleNextJob;
    else if(IrrigationManagerState::Run != irr_mgr_state)
      state = SchedulerState::ScheduleNextJob;
    break;
  }

  switch(opmode)
  {
  case OperationMode::Auto:
    requested_job = sched_job;
    break;
  case OperationMode::ManualOff:
    requested_job = 0xff;
    break;
  case OperationMode::Manual0:
    requested_job = 0;
    break;
  case OperationMode::Manual1:
    requested_job = 1;
    break;
  case OperationMode::Manual2:
    requested_job = 2;
    break;
  case OperationMode::Manual3:
    requested_job = 3;
    break;
  }

  pollIrrigationManager(intf, current_time);
}

JobNumberType
IrrigationProgram::getNextJobNumber(const PlcDateType &date) const
{
  JobNumberType selected;
  JobNumberType i;

  selected = 0xFF;
  for(i = 0; i < 4; ++i)
  {
    bool expired;

    expired = (date >= (last_job_exec[i] + PlcDateDeltaType({0, 0, parameters.job_interval[i]})));

    if(expired && (parameters.job_duration[i] <= irrigation_duration_budget) &&
                  (parameters.job_duration[i] > 0))
    { /* job is expired and we have enough budget to run it */
      if(selected < 4)
      {
        if(last_job_exec[selected] > last_job_exec[i])
          selected = i;
      }
      else
      {
        selected = i;
      }
    }
  }

  return selected;
}

void
IrrigationProgram::pollIrrigationManager(PlcProgramInterface &intf, const PlcDateTimeType & current_time)
{
  bool enable_pump;

  switch(irr_mgr_state)
  {
  case IrrigationManagerState::Idle:
    if(intf.asupply().value < parameters.battery_full_detected )
    {
      irrigation_timer = current_time.getTime();
    }
    else
    {
      if( (irrigation_timer + PlcTimeDeltaType({0,30,0})) < current_time.getTime())
        irrigation_duration_budget = parameters.max_duration_per_day;
    }

    /* fall through by intention */
  case IrrigationManagerState::Activate:
    if (requested_job < 4)
    {
      irrigation_timer = current_time.getTime() + PlcTimeDeltaType({0,0,15});
      irr_mgr_state = IrrigationManagerState::Starting;

      active_job = requested_job;
    }
    else if(active_job < 4)
    {
      irr_mgr_state = IrrigationManagerState::Shutdown;
      irrigation_timer = current_time.getTime() + PlcTimeDeltaType({0,0,15});
    }
    else
    {
      irr_mgr_state = IrrigationManagerState::Idle;
    }
    break;
  case IrrigationManagerState::Starting:
    if (requested_job != active_job)
    {
      irr_mgr_state = IrrigationManagerState::Activate;
    }
    else if(irrigation_timer < current_time.getTime())
    {
      irr_mgr_state = IrrigationManagerState::Run;
      irrigation_timer = current_time.getTime() + PlcTimeDeltaType({0,parameters.job_duration[active_job],0});
    }
    break;
  case IrrigationManagerState::Run:
    if (requested_job != active_job)
    {
      irr_mgr_state = IrrigationManagerState::Activate;
    }
    else if( irrigation_timer < current_time.getTime())
    {
      last_job_exec[active_job] = current_time.getDate();
      irr_mgr_state = IrrigationManagerState::Overrun;
    }
    break;

  case IrrigationManagerState::Overrun:
    if (requested_job != active_job)
    {
      irr_mgr_state = IrrigationManagerState::Activate;
    }
    break;

  case IrrigationManagerState::Shutdown:
    if(irrigation_timer < current_time.getTime())
    {
      active_job = 0xFF;
      irr_mgr_state = IrrigationManagerState::Idle;
    }
    break;
  }


  intf.dq(0).value = (active_job == 0) ? 1 : 0;
  intf.dq(1).value = (active_job == 1) ? 1 : 0;
  intf.dq(2).value = (active_job == 2) ? 1 : 0;
  intf.dq(3).value = (active_job == 3) ? 1 : 0;

  if (active_job < 4)
  {
    enable_pump = (irr_mgr_state != IrrigationManagerState::Idle) &&
                  (irr_mgr_state != IrrigationManagerState::Shutdown);
  }
  else
  {
    enable_pump = false;
  }

  if(enable_pump)
  {
    /* store timestamp when pump initially activated */
    if(intf.dq(7).value == 0)
      irrigation_ts = current_time.getTime();

    intf.dq(7).value = 1;

    /* Charge running time of pump */
    while((irrigation_duration_budget > 0) && (irrigation_ts < current_time))
    {
      if(irrigation_ts.add({0,1,0}))
      {
        irrigation_duration_budget = 0;
        break;
      }

      irrigation_duration_budget -= 1;
    }
  }
  else
  {
    intf.dq(7).value = 0;
  }
}



static const char* opmode_names[] =
{
  "Automatik",
  "Aus",
  "Kanal 1",
  "Kanal 2",
  "Kanal 3",
  "Kanal 4"
};

static constexpr EnumerationVariableMeta<uint_least8_t> opmode_meta(0,5, opmode_names);
static constexpr PrimitiveVariableMeta<uint_least8_t> read_only_meta(0xFF, 0);
static constexpr PrimitiveVariableMeta<uint_least8_t> job_duration_meta(0, 90);
static constexpr PrimitiveVariableMeta<uint_least8_t> job_interval_meta(1, 14);
static constexpr PrimitiveVariableMeta<uint_least8_t> max_duration_meta(0, 120);
static constexpr AnalogVariableMeta    min_rebudget_supply_voltage_meta(12.0, 16.0);
static constexpr DateVariableMeta      date_meta;

VariableIdType
IrrigationProgram::getVariableCount() const
{
  return 16;
}

VariableManager*
IrrigationProgram::getVariableManager(PlcParameterInterface & param_intf, VariableIdType var_id)
{
  switch(var_id)
  {
  case  0: return  param_intf.createVariableManager(opmode_meta, "Betriebsart", reinterpret_cast<uint8_t*>(&opmode));
  case  1: return  param_intf.createVariableManager(date_meta, "Zuletzt 1", last_job_exec[0]);
  case  2: return  param_intf.createVariableManager(date_meta, "Zuletzt 2", last_job_exec[1]);
  case  3: return  param_intf.createVariableManager(date_meta, "Zuletzt 3", last_job_exec[2]);
  case  4: return  param_intf.createVariableManager(date_meta, "Zuletzt 4", last_job_exec[3]);

  case  5: return  param_intf.createVariableManager(read_only_meta,     "Budget",    irrigation_duration_budget);

  case  6: return param_intf.createVariableManager(job_duration_meta, "Dauer 1",   parameters.job_duration[0]);
  case  7: return param_intf.createVariableManager(job_interval_meta, "Abstand 1", parameters.job_interval[0]);
  case  8: return param_intf.createVariableManager(job_duration_meta, "Dauer 2",   parameters.job_duration[1]);
  case  9: return param_intf.createVariableManager(job_duration_meta, "Abstand 2", parameters.job_interval[1]);
  case 10: return param_intf.createVariableManager(job_duration_meta, "Dauer 3",   parameters.job_duration[2]);
  case 11: return param_intf.createVariableManager(job_duration_meta, "Abstand 3", parameters.job_interval[2]);
  case 12: return param_intf.createVariableManager(job_duration_meta, "Dauer 4",   parameters.job_duration[3]);
  case 13: return param_intf.createVariableManager(job_duration_meta, "Abstand 4", parameters.job_interval[3]);

  case 14: return param_intf.createVariableManager(max_duration_meta,  "Laufzeit",  parameters.max_duration_per_day);
  case 15: return param_intf.createVariableManager(min_rebudget_supply_voltage_meta, "Ladespg.",  parameters.battery_full_detected);

  default: return nullptr;
  }
}


Program*
create_program(void *p_mem, unsigned int mem_len)
{
  if (mem_len < sizeof(IrrigationProgram))
    return nullptr;

  return new (p_mem) IrrigationProgram();
}

