#! /usr/bin/env python
# encoding: utf-8
TOP = '.'

def options(opt):
    opt.recurse(['firmware'])

def configure(conf):
    conf.recurse(['firmware'])

def build(ctx):
    ctx.recurse(['firmware'])